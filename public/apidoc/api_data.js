define({ "api": [
  {
    "type": "post",
    "url": "/api/cms/article/read",
    "title": "文章详情",
    "name": "__",
    "group": "Article",
    "version": "1.0.0",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Integer",
            "optional": false,
            "field": "appid",
            "description": "<p>小程序id</p>"
          },
          {
            "group": "Parameter",
            "type": "Integer",
            "optional": false,
            "field": "id",
            "description": "<p>文章id</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "200": [
          {
            "group": "200",
            "type": "Array",
            "optional": false,
            "field": "articleDetail",
            "description": "<p>文章数组</p>"
          },
          {
            "group": "200",
            "type": "String",
            "optional": false,
            "field": "articleDetail:title",
            "description": "<p>文章标题</p>"
          },
          {
            "group": "200",
            "type": "String",
            "optional": false,
            "field": "articleDetail:description",
            "description": "<p>文章描述</p>"
          },
          {
            "group": "200",
            "type": "String",
            "optional": false,
            "field": "articleDetail:content",
            "description": "<p>文章内容</p>"
          },
          {
            "group": "200",
            "type": "String",
            "optional": false,
            "field": "articleDetail:photos",
            "description": "<p>文章相册</p>"
          },
          {
            "group": "200",
            "type": "String",
            "optional": false,
            "field": "articleDetail:seo_title",
            "description": "<p>seo标题</p>"
          },
          {
            "group": "200",
            "type": "String",
            "optional": false,
            "field": "articleDetail:seo_keywords",
            "description": "<p>seo关键字</p>"
          },
          {
            "group": "200",
            "type": "String",
            "optional": false,
            "field": "articleDetail:seo_description",
            "description": "<p>seo描述</p>"
          },
          {
            "group": "200",
            "type": "String",
            "optional": false,
            "field": "articleDetail:is_top",
            "description": "<p>是否置顶</p>"
          },
          {
            "group": "200",
            "type": "String",
            "optional": false,
            "field": "articleDetail:is_recommend",
            "description": "<p>是否推荐</p>"
          },
          {
            "group": "200",
            "type": "String",
            "optional": false,
            "field": "articleDetail:is_release",
            "description": "<p>是否发布</p>"
          },
          {
            "group": "200",
            "type": "String",
            "optional": false,
            "field": "articleDetail:thumbnail",
            "description": "<p>缩略图</p>"
          },
          {
            "group": "200",
            "type": "String",
            "optional": false,
            "field": "articleDetail:update_time",
            "description": "<p>更新时间</p>"
          },
          {
            "group": "200",
            "type": "String",
            "optional": false,
            "field": "articleDetail:click_count",
            "description": "<p>点击量</p>"
          }
        ]
      }
    },
    "filename": "app/api/controller/cms/Article.php",
    "groupTitle": "Article"
  },
  {
    "type": "post",
    "url": "/api/cms/article/index",
    "title": "列表",
    "name": "____",
    "group": "Article",
    "version": "1.0.0",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Integer",
            "optional": false,
            "field": "appid",
            "description": "<p>小程序id</p>"
          },
          {
            "group": "Parameter",
            "type": "Integer",
            "optional": false,
            "field": "cid",
            "description": "<p>分类id</p>"
          },
          {
            "group": "Parameter",
            "type": "Integer",
            "optional": false,
            "field": "index",
            "description": "<p>位置</p>"
          },
          {
            "group": "Parameter",
            "type": "Integer",
            "optional": false,
            "field": "size",
            "description": "<p>数量</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "200": [
          {
            "group": "200",
            "type": "Array",
            "optional": false,
            "field": "articleList",
            "description": "<p>文章数组</p>"
          },
          {
            "group": "200",
            "type": "String",
            "optional": false,
            "field": "articleList:title",
            "description": "<p>文章标题</p>"
          },
          {
            "group": "200",
            "type": "String",
            "optional": false,
            "field": "articleList:description",
            "description": "<p>文章描述</p>"
          },
          {
            "group": "200",
            "type": "String",
            "optional": false,
            "field": "articleList:thumbnail",
            "description": "<p>文章缩略图</p>"
          },
          {
            "group": "200",
            "type": "String",
            "optional": false,
            "field": "articleList:update_time",
            "description": "<p>文章更新时间</p>"
          },
          {
            "group": "200",
            "type": "String",
            "optional": false,
            "field": "articleList:click_count",
            "description": "<p>文章点击数</p>"
          }
        ]
      }
    },
    "filename": "app/api/controller/cms/Article.php",
    "groupTitle": "Article"
  },
  {
    "type": "post",
    "url": "/api/cms/category/read",
    "title": "分类列表",
    "name": "____",
    "group": "Category",
    "version": "1.0.0",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Integer",
            "optional": false,
            "field": "appid",
            "description": "<p>小程序id</p>"
          },
          {
            "group": "Parameter",
            "type": "Integer",
            "optional": false,
            "field": "id",
            "description": "<p>分类id</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "200": [
          {
            "group": "200",
            "type": "Array",
            "optional": false,
            "field": "categoryList",
            "description": "<p>分类数组</p>"
          },
          {
            "group": "200",
            "type": "Array",
            "optional": false,
            "field": "categoryList:children",
            "description": "<p>子分类数组</p>"
          },
          {
            "group": "200",
            "type": "String",
            "optional": false,
            "field": "categoryList:name",
            "description": "<p>分类名称</p>"
          },
          {
            "group": "200",
            "type": "String",
            "optional": false,
            "field": "categoryList:path",
            "description": "<p>分类路径</p>"
          }
        ]
      }
    },
    "filename": "app/api/controller/cms/Category.php",
    "groupTitle": "Category"
  },
  {
    "type": "post",
    "url": "/api/cms/form/read",
    "title": "表单详情",
    "name": "____",
    "group": "Form",
    "version": "1.0.0",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Integer",
            "optional": false,
            "field": "appid",
            "description": "<p>小程序id</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "200": [
          {
            "group": "200",
            "type": "Array",
            "optional": false,
            "field": "FormDetail",
            "description": "<p>表单数组</p>"
          },
          {
            "group": "200",
            "type": "String",
            "optional": false,
            "field": "FormDetail:name",
            "description": "<p>表单名称</p>"
          },
          {
            "group": "200",
            "type": "Array",
            "optional": false,
            "field": "FormDetail:config",
            "description": "<p>表单配置</p>"
          },
          {
            "group": "200",
            "type": "String",
            "optional": false,
            "field": "FormDetail:config:name",
            "description": "<p>配置名称</p>"
          },
          {
            "group": "200",
            "type": "String",
            "optional": false,
            "field": "FormDetail:config:length",
            "description": "<p>配置长度</p>"
          },
          {
            "group": "200",
            "type": "String",
            "optional": false,
            "field": "FormDetail:config:field",
            "description": "<p>配置类型</p>"
          },
          {
            "group": "200",
            "type": "String",
            "optional": false,
            "field": "FormDetail:config:require",
            "description": "<p>是否必填</p>"
          }
        ]
      }
    },
    "filename": "app/api/controller/cms/Form.php",
    "groupTitle": "Form"
  },
  {
    "type": "post",
    "url": "/api/cms/form/message",
    "title": "留言",
    "name": "____",
    "group": "Form",
    "version": "1.0.0",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "appid",
            "description": "<p>小程序id</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "200": [
          {
            "group": "200",
            "type": "Array",
            "optional": false,
            "field": "data",
            "description": "<p>提交返回</p>"
          }
        ]
      }
    },
    "filename": "app/api/controller/cms/Form.php",
    "groupTitle": "Form"
  },
  {
    "type": "post",
    "url": "/api/cms/index",
    "title": "首页",
    "name": "__",
    "group": "Index",
    "version": "1.0.0",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Integer",
            "optional": false,
            "field": "appid",
            "description": "<p>小程序id</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "200": [
          {
            "group": "200",
            "type": "Array",
            "optional": false,
            "field": "bannerList",
            "description": "<p>幻灯片数组</p>"
          },
          {
            "group": "200",
            "type": "String",
            "optional": false,
            "field": "bannerList:id",
            "description": "<p>幻灯片ID</p>"
          },
          {
            "group": "200",
            "type": "String",
            "optional": false,
            "field": "bannerList:name",
            "description": "<p>幻灯片名称</p>"
          },
          {
            "group": "200",
            "type": "String",
            "optional": false,
            "field": "bannerList:image",
            "description": "<p>幻灯片图片</p>"
          },
          {
            "group": "200",
            "type": "Array",
            "optional": false,
            "field": "advList",
            "description": "<p>广告数组</p>"
          },
          {
            "group": "200",
            "type": "String",
            "optional": false,
            "field": "advList:id",
            "description": "<p>广告ID</p>"
          },
          {
            "group": "200",
            "type": "String",
            "optional": false,
            "field": "advList:name",
            "description": "<p>广告名称</p>"
          },
          {
            "group": "200",
            "type": "String",
            "optional": false,
            "field": "advList:category_id",
            "description": "<p>展现位置</p>"
          },
          {
            "group": "200",
            "type": "String",
            "optional": false,
            "field": "advList:image",
            "description": "<p>广告图片</p>"
          },
          {
            "group": "200",
            "type": "String",
            "optional": false,
            "field": "advList:skip_id",
            "description": "<p>广告跳转ID</p>"
          },
          {
            "group": "200",
            "type": "String",
            "optional": false,
            "field": "advList:skip_type",
            "description": "<p>广告跳转类型</p>"
          },
          {
            "group": "200",
            "type": "Array",
            "optional": false,
            "field": "noticeList",
            "description": "<p>公告数组</p>"
          },
          {
            "group": "200",
            "type": "String",
            "optional": false,
            "field": "noticeList:id",
            "description": "<p>公告ID</p>"
          },
          {
            "group": "200",
            "type": "String",
            "optional": false,
            "field": "noticeList:title",
            "description": "<p>公告标题</p>"
          },
          {
            "group": "200",
            "type": "String",
            "optional": false,
            "field": "noticeList:update_time",
            "description": "<p>更新时间</p>"
          },
          {
            "group": "200",
            "type": "Array",
            "optional": false,
            "field": "navList",
            "description": "<p>导航数组</p>"
          },
          {
            "group": "200",
            "type": "String",
            "optional": false,
            "field": "navList:id",
            "description": "<p>导航ID</p>"
          },
          {
            "group": "200",
            "type": "String",
            "optional": false,
            "field": "navList:type",
            "description": "<p>导航类型</p>"
          },
          {
            "group": "200",
            "type": "String",
            "optional": false,
            "field": "navList:name",
            "description": "<p>导航名称</p>"
          },
          {
            "group": "200",
            "type": "String",
            "optional": false,
            "field": "navList:icon",
            "description": "<p>导航图标</p>"
          },
          {
            "group": "200",
            "type": "Array",
            "optional": false,
            "field": "categoryList",
            "description": "<p>分类数组</p>"
          },
          {
            "group": "200",
            "type": "String",
            "optional": false,
            "field": "categoryList:id",
            "description": "<p>分类ID</p>"
          },
          {
            "group": "200",
            "type": "String",
            "optional": false,
            "field": "categoryList:type",
            "description": "<p>分类类型</p>"
          },
          {
            "group": "200",
            "type": "String",
            "optional": false,
            "field": "categoryList:name",
            "description": "<p>分类名称</p>"
          },
          {
            "group": "200",
            "type": "Array",
            "optional": false,
            "field": "categoryList:article",
            "description": "<p>分类下的文章</p>"
          },
          {
            "group": "200",
            "type": "Array",
            "optional": false,
            "field": "categoryList:article:id",
            "description": "<p>文章ID</p>"
          },
          {
            "group": "200",
            "type": "Array",
            "optional": false,
            "field": "categoryList:article:description",
            "description": "<p>文章描述</p>"
          },
          {
            "group": "200",
            "type": "Array",
            "optional": false,
            "field": "categoryList:article:thumbnail",
            "description": "<p>文章缩略图</p>"
          },
          {
            "group": "200",
            "type": "Array",
            "optional": false,
            "field": "categoryList:article:update_time",
            "description": "<p>文章更新时间</p>"
          },
          {
            "group": "200",
            "type": "Array",
            "optional": false,
            "field": "categoryList:article:click_count",
            "description": "<p>文章点击量</p>"
          }
        ]
      }
    },
    "filename": "app/api/controller/cms/Index.php",
    "groupTitle": "Index"
  },
  {
    "type": "post",
    "url": "/api/cms/info",
    "title": "基本信息",
    "name": "____",
    "group": "Index",
    "version": "1.0.0",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Integer",
            "optional": false,
            "field": "appid",
            "description": "<p>小程序id</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "200": [
          {
            "group": "200",
            "type": "Array",
            "optional": false,
            "field": "siteInfo",
            "description": "<p>站点信息数组</p>"
          },
          {
            "group": "200",
            "type": "String",
            "optional": false,
            "field": "siteInfo:name",
            "description": "<p>站点名称</p>"
          },
          {
            "group": "200",
            "type": "String",
            "optional": false,
            "field": "siteInfo:tel",
            "description": "<p>电话号码</p>"
          },
          {
            "group": "200",
            "type": "String",
            "optional": false,
            "field": "siteInfo:bg",
            "description": "<p>背景</p>"
          },
          {
            "group": "200",
            "type": "String",
            "optional": false,
            "field": "siteInfo:logo",
            "description": "<p>站点logo</p>"
          },
          {
            "group": "200",
            "type": "String",
            "optional": false,
            "field": "siteInfo:address",
            "description": "<p>站点地址</p>"
          },
          {
            "group": "200",
            "type": "String",
            "optional": false,
            "field": "siteInfo:lng",
            "description": "<p>纬度</p>"
          },
          {
            "group": "200",
            "type": "String",
            "optional": false,
            "field": "siteInfo:lat",
            "description": "<p>经度</p>"
          },
          {
            "group": "200",
            "type": "String",
            "optional": false,
            "field": "siteInfo:content",
            "description": "<p>站点内容</p>"
          },
          {
            "group": "200",
            "type": "String",
            "optional": false,
            "field": "siteInfo:copyright",
            "description": "<p>站点版权</p>"
          },
          {
            "group": "200",
            "type": "Array",
            "optional": false,
            "field": "siteStyle",
            "description": "<p>站点样式数组</p>"
          },
          {
            "group": "200",
            "type": "String",
            "optional": false,
            "field": "siteStyle:nav_font",
            "description": "<p>导航字体颜色</p>"
          },
          {
            "group": "200",
            "type": "String",
            "optional": false,
            "field": "siteStyle:nav_color",
            "description": "<p>导航颜色</p>"
          },
          {
            "group": "200",
            "type": "String",
            "optional": false,
            "field": "siteStyle:tel_color",
            "description": "<p>电话颜色</p>"
          },
          {
            "group": "200",
            "type": "String",
            "optional": false,
            "field": "siteStyle:tel_icon_color",
            "description": "<p>电话图标颜色</p>"
          },
          {
            "group": "200",
            "type": "String",
            "optional": false,
            "field": "siteStyle:footer_color",
            "description": "<p>底部导航颜色</p>"
          },
          {
            "group": "200",
            "type": "String",
            "optional": false,
            "field": "siteStyle:message_color",
            "description": "<p>留言颜色</p>"
          },
          {
            "group": "200",
            "type": "String",
            "optional": false,
            "field": "siteStyle:category_color",
            "description": "<p>分类导航颜色</p>"
          },
          {
            "group": "200",
            "type": "Array",
            "optional": false,
            "field": "navList",
            "description": "<p>底部导航数组</p>"
          },
          {
            "group": "200",
            "type": "String",
            "optional": false,
            "field": "navList:name",
            "description": "<p>底部导航名称</p>"
          },
          {
            "group": "200",
            "type": "String",
            "optional": false,
            "field": "navList:icon",
            "description": "<p>图标</p>"
          },
          {
            "group": "200",
            "type": "String",
            "optional": false,
            "field": "navList:select_icon",
            "description": "<p>选中图标</p>"
          },
          {
            "group": "200",
            "type": "String",
            "optional": false,
            "field": "navList:skip_id",
            "description": "<p>跳转ID</p>"
          },
          {
            "group": "200",
            "type": "String",
            "optional": false,
            "field": "navList:path",
            "description": "<p>跳转地址</p>"
          }
        ]
      }
    },
    "filename": "app/api/controller/cms/Info.php",
    "groupTitle": "Index"
  },
  {
    "type": "post",
    "url": "/api/cms/Notice/read",
    "title": "通知详情",
    "name": "____",
    "group": "Notice",
    "version": "1.0.0",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Integer",
            "optional": false,
            "field": "appid",
            "description": "<p>小程序id</p>"
          },
          {
            "group": "Parameter",
            "type": "Integer",
            "optional": false,
            "field": "id",
            "description": "<p>通知id</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "200": [
          {
            "group": "200",
            "type": "Array",
            "optional": false,
            "field": "NoticeDetail",
            "description": "<p>通知数组</p>"
          },
          {
            "group": "200",
            "type": "String",
            "optional": false,
            "field": "NoticeDetail:id",
            "description": "<p>通知ID</p>"
          },
          {
            "group": "200",
            "type": "String",
            "optional": false,
            "field": "NoticeDetail:title",
            "description": "<p>通知标题</p>"
          },
          {
            "group": "200",
            "type": "String",
            "optional": false,
            "field": "NoticeDetail:content",
            "description": "<p>通知内容</p>"
          },
          {
            "group": "200",
            "type": "String",
            "optional": false,
            "field": "NoticeDetail:seo_title",
            "description": "<p>seo标题</p>"
          },
          {
            "group": "200",
            "type": "String",
            "optional": false,
            "field": "NoticeDetail:seo_keywords",
            "description": "<p>seo关键字</p>"
          },
          {
            "group": "200",
            "type": "String",
            "optional": false,
            "field": "NoticeDetail:seo_description",
            "description": "<p>seo描述</p>"
          },
          {
            "group": "200",
            "type": "String",
            "optional": false,
            "field": "NoticeDetail:update_time",
            "description": "<p>更新时间</p>"
          }
        ]
      }
    },
    "filename": "app/api/controller/cms/Notice.php",
    "groupTitle": "Notice"
  }
] });
