/*
 * @Description: 后台公共JS
 * @Author: WangHeng
 * @Date: 2019-07-15 22:03:02
 * @LastEditTime: 2019-08-27 21:21:42
 */
layui.config({
    base: '/static/lib/layuinotice/'
});
layui.use(['layer','form','laydate','upload','notice'], function(){
    var form = layui.form;
    var laydate = layui.laydate;
    var upload = layui.upload;
    var notice = layui.notice;

    // 初始化配置，同一样式只需要配置一次，非必须初始化，有默认配置
    notice.options = {
        closeButton:false,//显示关闭按钮
        debug:false,//启用debug
        positionClass:"toast-top-right",//弹出的位置,
        showDuration:"300",//显示的时间
        hideDuration:"1000",//消失的时间
        timeOut:"2000",//停留的时间
        extendedTimeOut:"1000",//控制时间
        showEasing:"swing",//显示时的动画缓冲方式
        hideEasing:"linear",//消失时的动画缓冲方式
        iconClass: 'toast-info', // 自定义图标，有内置，如不需要则传空 支持layui内置图标/自定义iconfont类名
        onclick: null, // 点击关闭回调
    };
    
    laydate.render({
        elem: '#start'
    });
    laydate.render({
        elem: '#end'
    });
    form.on('checkbox(select-all)', function (data) {
        var status = data.elem.checked;
        $('.select-box').attr('checked', status);
        form.render();
    });

    // 图片上传
    upload.render({
        elem: '.upload-img'
        ,url : '/common/upload'
        ,field : 'uploadImg'
        ,before: function(){
            var item = this.item;
            var type = item.attr('type');
            this.data={'type':type};
        }
        ,done: function(res, index, upload){
            var item = this.item;
            item.children('input').val(res.src);
            item.children('.add-img').hide();
            item.children('.now-img').attr('src',res.src);
        }
    })

    // 相册上传
    var sort = 1;
    $('#upload-photo').click(function(){
        var photo_html = '<div class="upload-img" id="photo'+sort+'"><div class="del-photo" onclick="del_photo('+"'#photo"+sort+"'"+')" ><i class="layui-icon">&#xe640;</i></div><img src="" alt="" class="now-img" width="50"><input type="text" value="" name="photo[]" hidden></div>';
        $(this).before(photo_html);
        picupload("#photo" + sort);
        sort++;
    })
    function picupload(id) {
        upload.render({
            elem: id
            ,url: '/common/upload'
            ,data :{
                type:'p_slide',
            }
            ,field:'uploadImg'
            , before: function (obj) {
                obj.preview(function (index, file, result) {
                    $(id).children('img').attr('src',result);
                });
            }
            , done: function (res) {
                $(id).children('input').attr('value',res.src);
                $(id).children('img').attr('src',res.src);
            }
        });
    }

    // 提交
    $('.btn-submit').click(function(){
        layer.load(1,{time: 5*1000});
        var url = $(this).parents('.ajax-form').attr('action');
        var data = $(this).parents('.ajax-form').serializeArray();
        $.post(url,data,function(res){
            if(res.errcode == undefined){
                layer.closeAll();
                layer.msg(res.msg,{icon:res.data.status,time:600},function(){
                    if(res.data.is_load != undefined && res.data.is_load == 1){
                        window.location.reload();
                        return true;
                    }
                    if(res.data.is_load != undefined && res.data.is_load == 2){
                        return true;
                    }
                    if(res.url != undefined && res.url != ''){
                        window.location.href = res.url;
                    }else{
                        window.parent.location.reload();
                    }
                });
            }else{
                layer.closeAll();
                layer.msg(res.msg,{icon:res.status});
            }
        })
    })

    // 删除
    $('.btn-delete').click(function(){
        var url = $(this).data('href');
        var name = $(this).data('name');
        var that = $(this);
        layer.confirm('删除：'+name, {
            btn: ['删除','点错了']
          }, function(){
              layer.load(1,{time: 5*1000});
            $.post(url,function(res){
                layer.closeAll();
                if(res.data.status != 0){
                    that.parent().parent().remove();
                }
                layer.msg(res.msg,{icon: res.data.status});
            })
          }, function(){
        });
    })

    // 单按钮点击操作
    $('.btn-operate').click(function(){
        layer.load(1,{time: 5*1000});
        var url = $(this).data('href');
        $.post(url,function(res){
            layer.closeAll();
            layer.msg(res.msg,{icon: res.data.status,time:1000},function(){
                window.location.reload();
            });
        });
    })

    // 更新状态
    form.on('switch(status)', function(e){
        var isChecked = Number(e.elem.checked);
        $(this).val(isChecked);
        updateField($(this));
    });

    // 排序
    $('.x-sort').change(function(){
        updateField($(this));
    })

    // 更新值
    function updateField(that)
    {
        var id = that.data('id');
        var field = that.data('field');
        var value = that.val();
        var data = {
            id:id,
            field:field,
            value:value,
        };
        $.post('field',data,function(res){
            if(res.data.status == 1){
                notice.success(res.msg);
            }else{
                notice.error(res.msg);
            }
        });
    }

});

function delAll(argument) {
    var ids = [];
    $('tbody .select-box').each(function (index, el) {
        if ($(this).prop('checked')) {
            ids.push($(this).val())
        }
    });
    layer.confirm('确认要删除吗？', function (index) {
        var data = {
            id: ids.toString()
        };
        $.post('delete', data, function (res) {
            layer.msg(res.msg, { icon: res.data.status });
            if(res.data.status != 0){
                $(".layui-form-checked").not('.header').parents('tr').remove();
            }
        })
    });
}

function delRecyclebin(argument) {
    var ids = [];
    $('tbody .select-box').each(function (index, el) {
        if ($(this).prop('checked')) {
            ids.push($(this).val())
        }
    });
    layer.confirm('确认要删除吗？', function (index) {
        var data = {
            id: ids.toString()
        };
        $.post('recyclebinClear', data, function (res) {
            layer.msg(res.msg, { icon: res.data.status });
            if(res.data.status != 0){
                $(".layui-form-checked").not('.header').parents('tr').remove();
            }
        })
    });
}


