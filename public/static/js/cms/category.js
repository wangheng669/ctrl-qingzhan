layui.use(['form'], function(){
    var form = layui.form;
    $(document).on('click','.add-config',function(){
        var configTemplate = $(".config-template:last").prop("outerHTML");
        $(this).parents('.config-template').after(configTemplate);
        form.render('select');
    });
    $(document).on('click','.del-config',function(){
        var count = $('.config-template').length;
        if(count!=1){
            $(this).parents('.config-template').remove();
        }
    });
})