/*
 * @Description: 文章JS
 * @Author: WangHeng
 * @Date: 2019-08-16 20:43:35
 * @LastEditTime: 2019-08-27 22:29:17
 */
var ue = UE.getEditor('editor');
var description = UE.getEditor('description',{
    autoHeightEnabled: true,
    autoFloatEnabled: true
});
$('.del-photo').click(function(){
    del_photo($(this).parent('div'));
})
function del_photo(id){
    $(id).remove();
    event.stopPropagation();
}
var type = $('#category_ids option:selected').attr('title');
$('.img-prompt').html('推荐尺寸 '+categoryConfig[type].img);
type==6||type==5?$('#url').show():$('#url').hide();
layui.use('form', function(){
    var form = layui.form;
    form.on('select(category)', function(data){
        type = data.elem[data.elem.selectedIndex].title;
        $('.img-prompt').html('推荐尺寸 '+categoryConfig[type].img);
        $('#upload-img').attr('type',type);
        // 如果音频或视频则输入链接
        type==6||type==5?$('#url').show():$('#url').hide();
    });
});