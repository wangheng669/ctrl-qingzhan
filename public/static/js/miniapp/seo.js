layui.use('form', function(){
    var form = layui.form;
    form.on('switch(web)', function(data){
      var status = Number(data.elem.checked);
      $.post('web',{status:status},function(res){
        layer.msg(res.msg,{icon: res.data.status,time:1000});
      })
    });
})
$('.sitemap-submit').click(function(){
  layer.load();
  var data = $(this).parents('.ajax-form').serializeArray();  
  $.post('sitemap',data,function(res){
    if(res.errcode != undefined){
      // 未绑定熊掌号
      if(res.errcode == 60005){
        layer.closeAll();
        layer.confirm('请先绑定熊掌ID', {
          btn: ['当前主体具有熊掌号','开发者平台绑定']
          }, function(){
            $.post('bindxzh',function(res){
              layer.msg(res.msg,{icon:res.status});
            })
          }, function(){
            window.open("https://smartprogram.baidu.com");
          }
        )
      }else{
        layer.closeAll();
        layer.msg(res.msg,{icon:res.status});
      }
    }
  })
})