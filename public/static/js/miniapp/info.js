$('.miniapp-name').change(function(){
    var name = $(this).val();
    var data = {
        name:name,
    };
    $.post('verifybdName',data,function(res){
        if(res.status != 0){
            $('.item-prompt').show();
            $('.name-data').hide();
            $('.info-prompt').text(res.msg);
        }else{
            $('.item-prompt').hide();
            $('.name-data').hide();
        }
        if(res.status == 4){
            $('.name-data').show();
        }
    })
});

layui.use(['layer','upload','form'], function(){
    var upload = layui.upload;
    var form = layui.form;
    // 修改头像
    upload.render({
        elem: '#modify-headimg'
        ,url: 'upload/'
        ,field:'nameImg'
        ,done: function(res){
            var item = this.item;
            item.children('.head-img').attr('src',res.img_url);
            item.children('.img_url').val(res.img_url);
        }
    })
    upload.render({
        elem: '.upload-name'
        , url: 'upload/',
        field: 'nameImg',
        done: function (res) {
            var img_html = '<div class="upload-name-box"><input type="text" value="' + res.img_url + '" name="nameImg[]"><img src="' + res.img_url + '" alt="" height="100%" width="auto"></div>'
            $('.upload-name').before(img_html);
        }
    });

    // 提交
    $('.info-submit').click(function(){
        var data = $('.modifybdName').serializeArray();
        $.post('modifybdName',data,function(res){
            // 修改成功
            if(res.status == 0){
                layer.msg('修改成功', { icon: 1 },function(){
                    location.reload();
                });
            }
            if(res.status != 0){
                $('.item-prompt').show();
                $('.name-data').hide();
                $('.info-prompt').text(res.msg);
            }
            if(res.status == 4){
                $('.name-data').show();
            }
        })
    })

});