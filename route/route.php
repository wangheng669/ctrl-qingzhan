<?php
// +----------------------------------------------------------------------
// | ThinkPHP [ WE CAN DO IT JUST THINK ]
// +----------------------------------------------------------------------
// | Copyright (c) 2006~2018 http://thinkphp.cn All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: liu21st <liu21st@gmail.com>
// +----------------------------------------------------------------------
Route::get('api/cms/index','api/cms.index/index')->allowCrossDomain();
Route::get('api/cms/info','api/cms.info/index')->allowCrossDomain();
Route::get('api/cms/article/index','api/cms.article/index')->allowCrossDomain();
Route::get('api/cms/article/read','api/cms.article/read')->allowCrossDomain();
Route::get('api/cms/category/read','api/cms.category/read')->allowCrossDomain();
Route::get('api/cms/notice/read','api/cms.notice/read')->allowCrossDomain();
Route::get('api/cms/form/read','api/cms.form/read')->allowCrossDomain();
Route::post('api/cms/form/message','api/cms.form/message')->allowCrossDomain();
Route::get('api/admin/login','api/admin.login/index')->allowCrossDomain();
Route::post('api/admin/init','api/admin.init/index')->allowCrossDomain();
Route::post('api/admin/update','api/admin.init/updateData')->allowCrossDomain();
Route::post('api/admin/template','api/admin.template/index')->allowCrossDomain();
Route::post('api/admin/notify/clear','api/admin.notify/clear')->allowCrossDomain();
Route::post('api/admin/notify/index','api/admin.notify/index')->allowCrossDomain();
Route::get('404',function(){return '页面错误,请联系管理员';});