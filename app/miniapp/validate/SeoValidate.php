<?php
/*
 * @Description: SEO校验
 * @Author: WangHeng
 * @Date: 2019-07-12 23:48:38
 */

namespace app\miniapp\validate;

use app\common\validate\BaseValidate;

class SeoValidate extends BaseValidate
{
    
	protected $rule = [
        'title' => 'require',
        'keywords' => 'require',
        'description' => 'require',
    ];
    
    
    protected $message = [
        'title.require' => 'SEO标题不能为空',
        'keywords.require' => 'SEO关键字不能为空',
        'description.require' => 'SEO描述不能为空',
    ];
}
