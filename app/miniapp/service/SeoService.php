<?php

namespace app\miniapp\service;

use app\miniapp\model\Seo as SeoModel;
use app\admin\model\Nav as NavModel;
use app\cms\model\Category as CategoryModel;
use app\cms\model\Notice as NoticeModel;
use app\cms\model\Article as ArticleModel;

class SeoService
{

    public function createSiteMap()
    {
        $miniapp_id = session('miniapp_id');
        $seoModel = new SeoModel();
        $paths = [];
        $file_path = 'user/sitemap/u'.$miniapp_id;
        $file_name = '/sitemap.txt';
        if (! is_dir($file_path)){
            mkdir($file_path,0777,true);
        }
        // 导航
        $navModel = new NavModel();
        $navList = $navModel->getDataList([]);
        foreach($navList as $nav){
            if($nav['path'] == '/pages/tabBarTwo/tabBarTwo'){
                $nav['path'] .= '?skip_id='.$nav['skip_id'];
            }
            $paths[] = $nav['path'];
        }
        // 分类
        $categoryModel = new CategoryModel();
        $categoryList = $categoryModel->getDataList([]);
        foreach($categoryList as $category){
            if($category['type'] != 3){
                $paths[] = '/pages/tabBarTwo/tabBarTwo?skip_id='.$category['id'];
            }
        }
        // 公告
        $noticeModel = new NoticeModel();
        $noticeList = $noticeModel->getDataList([]);
        foreach($noticeList as $notice){
            $paths[] = '/pages/detail/detail?id='.$notice['id'];
        }
        // 文章
        $articleModel = new ArticleModel();
        $articleList = $articleModel->getDataList([]);
        foreach($articleList as $article){
            $paths[] = '/pages/detail/detail?id='.$article['id'];
        }
        return implode(',',$paths);
    }

}