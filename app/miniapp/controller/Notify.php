<?php

/**
 * 通知
 * @author WangHeng
 */

namespace app\miniapp\controller;

use think\Request;
use app\common\controller\Base;
use app\miniapp\model\Notify as NotifyModel;

class Notify extends Base
{

    public function index()
    {
        $notifyModel = new NotifyModel();
        $notifyList = $notifyModel->where(['miniapp_id' => $this->miniapp_id])->paginate(10);
        $this->assign('notifyList',$notifyList);
        return $this->fetch();
    }

    // 更新字段值
    public function field(Request $request)
    {
        $data = $request->param();
        $notifyModel = new NotifyModel();
        $result = $notifyModel->updateField($data);
        $this->success($result['msg'],'',['status' => $result['status']]);
    }

}
