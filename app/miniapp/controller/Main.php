<?php

namespace app\miniapp\controller;

use think\Request;
use app\common\controller\Base;

class Main extends Base
{
    public function index()
    {
        $this->checkXcxEmpower();
        $xcxInfo = $this->getCache('xcxInfo');
        if(!$xcxInfo){
            $xcxInfo = $this->getPlatformData('miniapp.baidu.excerpt',[]);
            $this->setCache('xcxInfo',$xcxInfo);
        }
        $this->assign('xcxInfo',$xcxInfo);
        return $this->fetch();
    }
    
    // 上传代码
    public function uploadCode()
    {
        $this->getPlatformData('miniapp.baidu.upload',[]);
        $this->clearCache('xcxInfo');
        $this->success('上传成功','',['status' => 1]);
    }

    // 提交审核
    public function submitAudit(Request $request)
    {
        $data = $request->param();
        $this->getPlatformData('miniapp.baidu.submit',$data);
        $this->clearCache('xcxInfo');
        $this->success('提交成功','',['status' => 1,'is_load' => 1]);
    }

    // 审核撤回
    public function auditWithdrawal()
    {
        $this->getPlatformData('miniapp.baidu.withdraw',[]);
        $this->clearCache('xcxInfo');
        $this->success('撤回成功','',['status' => 1,'is_load' => 1]);
    }

    // 发布上线
    public function publishLunch()
    {
        $this->getPlatformData('miniapp.baidu.publish',[]);
        $this->clearCache('xcxInfo');
        $this->success('发布成功','',['status' => 1]);
    }

}
