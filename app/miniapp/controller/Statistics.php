<?php

/**
 * 统计
 * @author WangHeng
 */

namespace app\miniapp\controller;

use think\Request;
use app\common\controller\Base;

class Statistics extends Base
{

    public function index()
    {
        $this->checkXcxEmpower();
        $userTrends = $this->getUserTrends();
        $this->assign('userTrends',json_encode($userTrends));
        return $this->fetch();
    }

    // 获取用户趋势
    public function getUserTrends()
    {
        $data = request()->param();
        if(empty($data)){
            $data = [
                'start_index' => 1,
                'start_date' => '20190720',
                'end_date' => '20190725',
                'gran' => 'hour',
            ];
        }else{
            $data['time'] = explode(' - ',$data['time']);
            $data = [
                'start_index' => 1,
                'start_date' => $data['time'][0],
                'end_date' => $data['time'][1],
                'gran' => $data['gran'],
            ];
        }
        $result = $this->getPlatformData('miniapp.statistics.usertrends',$data);
        $legned = [
            'new_user_count' => '新用户数',
            'new_user_ratio' => '新用户占比',
            'old_user_count' => '老用户数',
            'old_user_ratio' => '老用户占比',
            'user_count' => '启动用户',
            'session_count' => '启动次数',
            // 'average_session_time' => '次均使用时长',
            // 'session_time_per_person' => '人均使用时长',
        ];
        $name = array_reverse(array_column($result['data_list'], 'name'));
        foreach($legned as $k=>$v){
            $data = array_reverse(array_column($result['data_list'], $k));
            $data = array_map(function($obj){
                if($obj == '--'){
                    return 0;
                }else{
                    return $obj;
                }
            },$data);
            $resultData[] = [
                'xAxis' => $name,
                'series' => $data,
            ];
        }
        $resultData['legned'] = $legned;
        return $resultData;
    }
    
    // 获取活跃用户
    public function getActiveUser()
    {
        $data = request()->param();
        if(empty($data)){
            $data = [
                'start_index' => 1,
                'start_date' => '20190720',
                'end_date' => '20190725',
            ];
        }else{
            $data['time'] = explode(' - ',$data['time']);
            $data = [
                'start_index' => 1,
                'start_date' => $data['time'][0],
                'end_date' => $data['time'][1],
            ];
        }
        $data['code'] = $this->getCode();
        $result = $this->getPlatformData('miniapp.statistics.activeuser',$data);
        $legned = [
            'user_count' => '启动用户',
            'daily_activity_degree' => '日活跃度',
            'day_month_activity_degree' => '日活/月活',
            'weekly_user_count' => '周活跃用户',
            'weekly_activity_degree' => '周活跃度',
            'monthly_user_count' => '月活跃用户',
            'monthly_activity_degree' => '月活跃度',
            'lost_user_count' => '流失用户',
            'lost_ratio' => '流失率',
            'accumulative_user_count' => '累计启动用户',
        ];
        $name = array_reverse(array_column($result['data_list'], 'name'));
        foreach($legned as $k=>$v){
            $data = array_reverse(array_column($result['data_list'], $k));
            $data = array_map(function($obj){
                if($obj == '--'){
                    return 0;
                }else{
                    return $obj;
                }
            },$data);
            $resultData[] = [
                'xAxis' => $name,
                'series' => $data,
            ];
        }
        $resultData['legned'] = $legned;
        return $resultData; 
    }

    // 获取活跃用户留存
    public function getUserRetention()
    {
        $data = request()->param();
        $data['time'] = explode(' - ',$data['time']);
        $data = [
            'start_index' => 1,
            'start_date' => $data['time'][0],
            'end_date' => $data['time'][1],
            'gran' => $data['gran'],
            'report_type' => 'count',
        ];
        $data['code'] = $this->getCode();
        $result = $this->getPlatformData('miniapp.statistics.userretention',$data);
        $name = array_keys($result['data_list'][0]);
        $name[0] = '日期';
        $name[1] = '活跃用户数';
        $resultData = [
            'name' => $name,
            'dataList' => $result['data_list'],
        ];
        return $resultData;
    }

    // 获取用户画像
    public function getUserPortrait()
    {
        $data = request()->param();
        $data['time'] = explode(' - ',$data['time']);
        $data = [
            'start_date' => $data['time'][0],
            'end_date' => $data['time'][1],
        ];
        $data['code'] = $this->getCode();
        $result = $this->getPlatformData('miniapp.statistics.userportrait',$data);
        $resultData = [];
        foreach($result['data_list'][0]['age'] as $v){
            $resultData['name'][] = $v['name'];
            $resultData['dataList'][] = $v['user_count_ratio'];
        }
        return $resultData;
    }
    
    // 获取用户分布
    public function getUserDistribution()
    {
        $data = request()->param();
        $data['time'] = explode(' - ',$data['time']);
        $data = [
            'start_index' => 1,
            'start_date' => $data['time'][0],
            'end_date' => $data['time'][1],
        ];
        $data['code'] = $this->getCode();
        $result = $this->getPlatformData('miniapp.statistics.userdistribution',$data);
        $resultData = [];
        foreach($result['data_list'] as $v){
            $resultData['name'][] = $v['name'];
            $resultData['dataList'][] = [
                'name' => $v['name'],
                'value' => $v['session_count_district'],
            ];
        }
        return $resultData;
    }
    
    // 获取终端分布
    public function getTerminalDistribution()
    {
        $data = request()->param();
        $data['time'] = explode(' - ',$data['time']);
        $data = [
            'start_index' => 1,
            'start_date' => $data['time'][0],
            'end_date' => $data['time'][1],
            'terminal_type' => $data['terminal_type'],
        ];
        $data['code'] = $this->getCode();
        $result = $this->getPlatformData('miniapp.statistics.terminaldistribution',$data);
        $name = array_column($result['data_list'], 'name');
        $legned = [
            'accumulative_session_count' => '启动次数',
            'accumulative_session_count_ratio' => '启动次数分布',
            'accumulative_new_user_count_ratio' => '新用户分布',
            'accumulative_user_count_ratio' => '启动用户分布',
            'average_use_time' => '次均使用时长',
        ];
        foreach($legned as $k=>$v){
            $data = array_column($result['data_list'], $k);
            $resultData[] = [
                'xAxis' => $name,
                'series' => $data,
            ];
        }
        return $resultData;
    }
    
    // 获取页面分析
    public function getPageAnalysis()
    {
        $data = request()->param();
        $data['time'] = explode(' - ',$data['time']);
        $data = [
            'start_index' => 1,
            'start_date' => $data['time'][0],
            'end_date' => $data['time'][1],
        ];
        $data['code'] = $this->getCode();
        $result = $this->getPlatformData('miniapp.statistics.pageanalysis',$data);
        $resultData = [
            'dataList' => $result['data_list'],
        ];
        return $resultData;
    }
    
    // 获取用户习惯
    public function getUserHabits()
    {
        $data = request()->param();
        $data['time'] = explode(' - ',$data['time']);
        $data = [
            'start_index' => 1,
            'start_date' => $data['time'][0],
            'end_date' => $data['time'][1],
            'character_type' => $data['character_type'],
        ];
        $data['code'] = $this->getCode();
        $result = $this->getPlatformData('miniapp.statistics.userhabits',$data);
        $key = array_keys($result['data_list'][0]);
        $resultData = [
            'name' => array_column($result['data_list'],'name'),
            'dataList' => array_column($result['data_list'],$key[1]),
        ];
        return $resultData;
    }
    
    // 获取来源分析
    public function getSourceanalysis()
    {
       
    }

}
