<?php

namespace app\miniapp\controller\wechat;

use think\Request;
use app\common\controller\Base;

class Main extends Base
{
    public function index()
    {
        $xcxInfo = $this->getCache('wxXcxInfo');
        if(!$xcxInfo){
            $xcxInfo = $this->getPlatformData('miniapp.wechat.excerpt',[]);
            $this->setCache('xcxInfo',$xcxInfo);
        }
        $this->assign('xcxInfo',$xcxInfo);
        return $this->fetch();
    }
 
    // 上传代码
    public function uploadCode()
    {
        $this->getPlatformData('miniapp.wechat.upload',[]);
        $this->clearCache('wxXcxInfo');
        $this->success('上传成功','',['status' => 1]);
    }
    
    // 上传代码
    public function submitAudit(Request $request)
    {
        $data = $request->param();
        $this->getPlatformData('miniapp.wechat.commit',$data);
        $this->clearCache('wxXcxInfo');
        $this->success('提交成功','',['status' => 1,'is_load' => 1]);
    }

    // 审核撤回
    public function auditWithdrawal()
    {
        $this->getPlatformData('miniapp.wechat.undocodeaudit',[]);
        $this->clearCache('wxXcxInfo');
        $this->success('撤回成功','',['status' => 1,'is_load' => 1]);
    }

    // 发布上线
    public function publishLunch()
    {
        $this->getPlatformData('miniapp.wechat.release',[]);
        $this->clearCache('wxXcxInfo');
        $this->success('发布成功','',['status' => 1]);
    }

    // 查看审核状态
    public function lookStatus()
    {
        $data = $this->getPlatformData('miniapp.wechat.lookstatus',[]);
        $this->clearCache('wxXcxInfo');
        $this->success($data,'',['status' => 1]);
    }

}
