<?php

namespace app\miniapp\controller;

use think\Request;
use app\common\controller\Base;
use app\miniapp\model\Seo as SeoModel;
use app\miniapp\validate\SeoValidate;
use app\miniapp\service\SeoService;

class Seo extends Base
{

    public function index(Request $request)
    {
        $this->checkXcxEmpower();
        $seoModel = new SeoModel();
        if($request->isPost()){
            (new SeoValidate())->goCheck($request);
            $data = $request->param();
            $seoModel->allowField(true)->update($data,['miniapp_id' => $this->miniapp_id]);
            $this->clearCache('siteData');
            $this->success('保存成功','',['status' => 1,'is_load' => 2]);
        }else{
            $seoDetail = $seoModel->getDataDetail([]);
            $this->assign('seoDetail',$seoDetail);
            return $this->fetch();
        }
    }

    // 生成站点地图
    public function sitemap(Request $request)
    {
        $data = $request->param();
        $seoService = new SeoService();
        $siteMap = $seoService->createSiteMap();
        // 提交站点地图
        $data = [
            'type' => $data['type'],
            'url_list' => $siteMap,
        ];
        $this->getPlatformData('miniapp.baidu.sitemap',$data);
        $this->success('生成成功','',['status' => 1,'is_load' => 2]);
    }

    // 绑定当前主体熊掌号
    public function bindxzh()
    {
        $this->getPlatformData('miniapp.baidu.bindxzh',[]);
        $this->success('绑定成功','',['status' => 1,'is_load' => 2]);
    }

    // 开启web化
    public function web(Request $request)
    {
        $seoModel = new SeoModel();
        $web_status = $request->post('status');
        $this->getPlatformData('miniapp.baidu.web',['web_status' => empty($web_status)?2:1]);
        $seoModel->update(['is_web' => $web_status],['miniapp_id' => $this->miniapp_id]);
        $this->clearCache('siteData');
        $this->success('设置成功','',['status' => 1,'is_load' => 2]);
    }

}
