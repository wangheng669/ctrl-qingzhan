<?php
/*
 * @Description: 小程序基础信息修改
 * @Author: WangHeng
 * @Date: 2019-08-08 23:00:37
 * @LastEditTime: 2019-09-08 22:20:41
 */
namespace app\miniapp\controller;

use app\common\controller\Base;
use think\Request;

class Info extends Base
{

    public function index()
    {
        $this->checkXcxEmpower();
        $baseInfo = $this->getXcxBaseInfo();
        $this->assign('baseInfo',$baseInfo);
        return $this->fetch();
    }

    // 校验小程序名称
    public function verifybdName(Request $request)
    {
        $name = $request->param('name');
        $result = $this->getPlatformData('miniapp.baidu.namecheck',['name' => $name]);
        return $result;
    }

    // 修改小程序头像
    public function modifybdheadImg(Request $request)
    {
        $imgUrl = $request->param('img_url');
        $this->getPlatformData('miniapp.baidu.modifyheadimg',['image_url' => $imgUrl,]);
        $this->success('保存成功','',['status' => 1,'is_load' => 1]);
    }

    // 修改小程序简介
    public function modifybdSignature(Request $request)
    {
        $desc = $request->param('desc');
        $this->getPlatformData('miniapp.baidu.signature',['signature' => $desc,]);
        $this->success('保存成功','',['status' => 1,'is_load' => 1]);
    }

    // 修改小程序名称
    public function modifybdName(Request $request)
    {
        $name = $request->param('name');
        $nameImg = $request->param('nameImg/a');
        $data = [
            'name' => $name,
        ];
        if($nameImg){
            $data['photo_url'] = implode(',',$nameImg);
        }
        $result = $this->getPlatformData('miniapp.baidu.modifyname',$data);
        return $result;
    }

    // 上传头像
    public function upload(Request $request)
    {
        $nameImg = $request->file('nameImg');
        $obj = curl_file_create(realpath($nameImg->getRealPath()),"image/jpeg",1);
        $data = [
            'type' => 3,
            'img_file' => $obj,
        ];
        $result = $this->getPlatformData('miniapp.baidu.uploadimg',$data);
        return $result;
    }

    // 获取小程序信息
    public function getXcxBaseInfo()
    {
        $result = $this->getPlatformData('miniapp.baidu.info',[]);
        return $result;
    }
    
    // 获取小程序服务类目列表
    public function getCategoryList()
    {
        $data = [
            'category_type' => 2,
        ];
        $this->getPlatformData('miniapp.bdcategory',$data);
        $this->success('保存成功','',['status' => 1,'is_load' => 1]);
    }

}
