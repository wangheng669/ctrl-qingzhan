<?php

namespace app\miniapp\controller;

use app\common\controller\Base;
use think\Request;

class Attestation extends Base
{
    
    public function index()
    {
        return $this->fetch();
    }

    // 人脸识别
    public function face()
    {
        $result = $this->getPlatformData('miniapp.baidu.attestation',[]);
        return $result;
    }

}