<?php

// 获取百度小程序状态
function bd_status($value)
{
    $status = '';
    if($value == 0){
        $status = '<span class="layui-badge-rim layui-bg-black">等待上传代码</span>';
    }
    if($value == 3){
        $status = '<span class="layui-badge-rim layui-bg-cyan">等待提交审核</span>';
    }
    if($value == 4){
        $status = '<span class="layui-badge-rim layui-bg-blue">等待百度审核</span>';
    }
    if($value == 6){
        $status = '<span class="layui-badge-rim layui-bg-orange">等待发布上线</span>';
    }
    if($value == 1){
        $status = '<span class="layui-badge-rim layui-bg-green">上线运营中</span>';
    }
    if($value == 5){
        $status = '<span class="layui-badge-rim layui-bg-gray">审核失败</span>';
    }
    return $status;
}

// 获取微信小程序状态
function wx_status($value)
{
    $status = '';
    if($value==0){
        $status = '<span class="layui-badge-rim layui-bg-black">等待上传代码</span>';
    }
    if($value==1){
        $status = '<span class="layui-badge-rim layui-bg-cyan">等待提交审核</span>';
    }
    if($value==2){
        $status = '<span class="layui-badge-rim layui-bg-blue">等待微信审核</span>';
    }
    if($value==3){
        $status = '<span class="layui-badge-rim layui-bg-orange">等待发布上线</span>';
    }
    if($value==8){
        $status = '<span class="layui-badge-rim layui-bg-green">上线运营中</span>';
    }
    if($value==4){
        $status = '<span class="layui-badge-rim layui-bg-gray">审核拒绝</span>';
    }
    return $status;
}

// 百度审核状态
function bd_audit_status($status)
{
    if($status == 1){
        return '审核中';
    }
    if($status == 3){
        return '审核失败';
    }
}