<?php
/*
 * @Description: 菜单
 * @Author: WangHeng
 * @Date: 2019-08-16 22:34:45
 * @LastEditTime: 2019-09-18 14:45:00
 */

namespace app\admin\service;

use app\admin\model\Menu as MenuModel;

class MenuService{

    // 添加菜单
    public function addMenu($data)
    {
        $menuModel = new MenuModel();
        $menu = $menuModel->allowField(true)->create($data);
        // 判断是否有父级菜单
        if(empty($data['pid'])){
            $menu->path = '0,'.$menu->id;
        }else{
            $parentPath = $menuModel->where('id',$data['pid'])->value('path');
            $menu->path = $parentPath.','.$menu->id;
            $menu->level = count(explode(',',$menu->path)) -2;
        }
        $menu->save();
    }

    // 编辑分类
    public function editMenu($data,$id)
    {
        $pid = $data['pid'];
        $menuModel = new MenuModel();
        $menuDetail = $menuModel->getDataDetail(['id' => $id],'*',0);
        // 判断是否存在父id
        if(empty($data['pid'])){
            $newPath = '0,'.$id;
        }else{
            $parentPath = $menuModel->where('id',$pid)->value('path');
            $newPath = $parentPath.','.$id;
        }
        $data['path'] = $newPath;
        $data['level'] = count(explode(',',$newPath)) -2;
        $condition[] = ['path','like',$menuDetail['path'].",%"];
        $childrenMenu = $menuModel->getDataList($condition,'id desc','id,path','',0);
        if ($childrenMenu) {
            foreach($childrenMenu as $menu){
                $childPath = str_replace($menuDetail['path'] . ',', $newPath . ',', $menu['path']);
                $level = count(explode(',',$menu['path'])) -2;
                $menuModel->update(['path' => $childPath,'level' => $level], ['id' => $menu['id']]);
            }
        }
        $menuModel->allowField(true)->update($data,['id' => $id]);
    }

    public function getMenuTree($condition)
    {
        $menuModel = new MenuModel();
        $menuList = $menuModel->getDataList($condition,'order asc','name,method,icon,pid,id','',0);
        $menuTree = $this->menuIndex($menuList);
        return $menuTree;
    }

    //返回所有菜单分类
    public function menuIndex($cate, $name = 'child', $pid = 0) {
        $arr = array();
        foreach ($cate as $v) {
            if ($v['pid'] == $pid) {
                $v[$name] = $this->menuIndex($cate, $name, $v['id']);
                $arr[] = $v;
            }
        }
        return $arr;
    }

    public function getMenuList()
    {
        $menuModel = new MenuModel();        
        $menuList = $menuModel->getDataList(['pid' => 0],'order asc','*','',0);
        foreach($menuList as $menu){
            $menu['children'] = $menuModel->getDataList([['path','like',$menu['path'].",%"]],'id desc','*','',0);
        }
        return $menuList;
    }


}