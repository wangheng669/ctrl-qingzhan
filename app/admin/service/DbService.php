<?php

namespace app\admin\service;

use think\Db;
use think\facade\App;
use app\admin\model\Db as DbModel;
use app\cms\model\Banner as BannerModel;
use app\cms\model\Notice as NoticeModel;
use app\cms\model\Adv as AdvModel;
use app\cms\model\Category as CategoryModel;
use app\cms\model\Article as ArticleModel;

class DbService{

    private $tables;

    private $miniapp_id;


    public function __construct()
    {
        $this->tables = [
            'adv','article','banner','category','form','message','nav','notice','role','seo','user'
        ];
        $this->miniapp_id = session('miniapp_id');
    }

    // 备份数据
    public function backupData($name)
    {
        $dataPath = App::getRootPath().'/backup/u'.$this->miniapp_id.'/';
        $fileName = date("Ymd-H-i-s",time()).'.txt';
        if (!is_dir($dataPath)){
            mkdir($dataPath,0777,true);
        }
        
        $sql = '';
        foreach($this->tables as $table){
            $result = Db::name($table)->where(['miniapp_id' => $this->miniapp_id])->select();
            foreach ($result as $row) {
                $row['delete_time'] = 0;
                $sql .= "INSERT INTO `ctrl_{$table}` VALUES ('" . implode("', '", $row) . "');\n\n";
            }
        }
        $filePath = $dataPath.'/'.$fileName;
        file_put_contents($filePath,$sql);
        $dbModel = new DbModel();
        $data = [
            'name' => $name,
            'path' => $filePath,
            'miniapp_id' => $this->miniapp_id,
            'create_time' => time(),
            'update_time' => time(),
        ];
        $dbModel->save($data);
    }

    // 导入数据
    public function importData($id)
    {
        $dbDetail = Db::name('db')->where(['id' => $id,'miniapp_id' => $this->miniapp_id])->find();
        $sqlArr = explode(";\n\n", file_get_contents($dbDetail['path']));
        Db::startTrans();
        try{
            $this->deleteData();
            foreach ($sqlArr as $sql) 
            {
                $sql = trim($sql);
                if(empty($sql)){
                    continue;
                }else{
                    Db::query($sql);
                }
            }
            Db::commit();
        }catch(Exception $e){
            Db::rollback();
            throw $e;
        }
    }

    // 删除之前的数据
    public function deleteData()
    {
        foreach($this->tables as $table){
            Db::name($table)->where(['miniapp_id' => $this->miniapp_id])->delete();
        }
    }

    // 复制数据
    public function copyData($miniapp_id)
    {
        $condition = [
            'miniapp_id' => $miniapp_id,
            'delete_time' => 0,
        ];
        Db::startTrans();
        try{
            // 复制幻灯片
            $bannerList = Db::name('banner')->where($condition)->select();
            foreach($bannerList as $banner){
                unset($banner['id']);
                $banner['miniapp_id'] = $this->miniapp_id;
                Db::name('banner')->insert($banner);
            }
            // 复制公告
            $noticeList = Db::name('notice')->where($condition)->select();
            foreach($noticeList as $notice){
                unset($notice['id']);
                $notice['miniapp_id'] = $this->miniapp_id;
                Db::name('notice')->insert($notice);
            }

            // 复制分类
            $categoryList = Db::name('category')->where($condition)->select();
            $cIds = [];
            foreach($categoryList as $category){
                if($category['pid']){
                    $category['pid'] = $cIds[$category['pid']];
                }
                $cOldID = $category['id'];
                unset($category['id']);
                $category['field'] = json_encode($category['field'],1);
                $category['miniapp_id'] = $this->miniapp_id;
                $cIds[$cOldID] = Db::name('category')->insertGetId($category);
            }

            $categoryList = Db::name('category')->where(['miniapp_id' => $this->miniapp_id,'delete_time' => 0])->select();
            foreach($categoryList as $category){
                $newPath = [];
                $paths = explode(',',$category['path']);
                foreach($paths as $path){
                    if(isset($cIds[$path])){
                        $newPath[] = $cIds[$path];
                    }
                }
                if($newPath){
                    Db::name('category')->where(['id' => $category['id']])->update(['path' => '0,'.implode(',',$newPath)]);
                }
            }            
            // 复制文章
            $articleList = Db::name('article')->where($condition)->select();
            $aIds = [];
            foreach($articleList as $article){
                $aOldId = $article['id'];
                // 替换分类Id
                $oldC = explode(',',$article['category_ids']);
                $replaceC = [];
                foreach($oldC as $c){
                    if(isset($cIds[$c])){
                        $replaceC[] = $cIds[$c];
                    }
                }
                $replaceC = implode(',',$replaceC);
                unset($article['id']);
                $article['category_ids'] = $replaceC;
                $article['miniapp_id'] = $this->miniapp_id;
                $aIds[$aOldId] = Db::name('article')->insertGetId($article);
            }
            // 复制广告
            $advList = Db::name('adv')->where($condition)->select();
            foreach($advList as $adv){
                unset($adv['id']);
                if($adv['skip_type'] == 1){
                   $adv['skip_id'] = $cIds[$adv['skip_id']];
                }else{
                   $adv['skip_id'] = $aIds[$adv['skip_id']];                    
                }
                if($adv['category_id']){
                    $adv['category_id'] = $cIds[$adv['category_id']];
                }
                $adv['miniapp_id'] = $this->miniapp_id;
                Db::name('adv')->insert($adv);
            }
        } catch (\Exception $e){
            Db::rollback();
            throw $e;
        }
        Db::commit();
    }

}