<?php
/*
 * @Description: 登录校验
 * @Author: WangHeng
 * @Date: 2019-07-12 23:48:38
 */

namespace app\admin\validate;

use app\common\validate\BaseValidate;

class LoginValidate extends BaseValidate
{
    
	protected $rule = [
        'login' => 'require',
        'password' => 'require',
    ];
    
    
    protected $message = [
        'login.require' => '请填写用户名',
        'password.require' => '请填写密码',
    ];
}
