<?php
/*
 * @Description: 菜单验证
 * @Author: WangHeng
 * @LastEditTime: 2019-08-16 21:05:55
 */
namespace app\admin\validate;

use app\common\validate\BaseValidate;

class MenuValidate extends BaseValidate
{
    
	protected $rule = [
        'pid' => 'require',
        'name' => 'require',
        'icon' => 'require',
        'status' => 'require',
    ];
    
    
    protected $message = [
        'pid.require' => '请填写上级菜单',
        'name.require' => '请填写菜单名称',
        'icon.require' => '请填写菜单图标',
        'status.require' => '请填写菜单状态',
    ];
}
