<?php
/*
 * @Description: 导航验证
 * @Author: WangHeng
 * @LastEditTime: 2019-08-04 17:56:57
 */
namespace app\admin\validate;

use app\common\validate\BaseValidate;

class NavValidate extends BaseValidate
{
    
	protected $rule = [
        'name' => 'require',
        'skip_id' => 'require',
        'icon' => 'require',
        'select_icon' => 'require',
        'status' => 'require',
    ];
    
    
    protected $message = [
        'name.require' => '请填写导航名称',
        'skip_id.require' => '请选择导航跳转位置',
        'icon.require' => '请填写导航图标',
        'select_icon.require' => '请填写导航选中图标',
        'status.require' => '请选择导航状态',
    ];
}
