<?php
/*
 * @Description: 登录
 * @Author: wangheng
 * @Date: 2019-07-11 20:54:38
 * @LastEditTime: 2019-09-05 17:08:46
 * @LastEditors: Please set LastEditors
 */

namespace app\admin\controller;

use think\Controller;
use think\Request;
use app\common\service\UserService;
use app\admin\validate\LoginValidate;

class Login extends Controller
{
    /**
     * 后台登陆界面
     * @author WangHeng
     */
    public function index(Request $request)
    {
        $userService = new UserService();
        // 校验是否已登录
        if($userService->isLogin()){
            return $this->redirect('index/index');
        }
        if($request->isPost()){
            (new LoginValidate())->goCheck($request); 
            // 校验登录
            $userService->loginVerify($request);
            // 获取用户规则
            $menuList = $userService->getUserMenu();
            session('menuList',$menuList);
            return $this->success('登录成功','index/index');
        }
        return $this->fetch();
    }

    /**
     * 用户退出
     * @author WangHeng
     */
    public function logout()
    {
        session(null);
        return $this->redirect('login/index');
    }
}
