<?php
/*
 * @ 导航管理
 * @Author: WangHeng
 * @Date: 2019-07-31 21:33:09
 * @LastEditTime: 2019-08-19 16:05:58
 */

namespace app\admin\controller;

use think\Request;
use app\common\controller\Base;
use app\admin\model\Nav as NavModel;
use app\cms\model\Category as CategoryModel;
use app\admin\validate\NavValidate;

class Nav extends Base
{

    /**
     * 初始操作
     * @author WangHeng
     */
    protected function initialize()
    {
        parent::initialize();
        // 获取模板配置
        $condition[] = ['type','in','b_nav'];
        $this->getTemplateConfig($condition,1);
    }

    public function index(Request $request)
    {
        $data = $request->param();
        $condition = [];
        $start = empty($data['start'])?'':$data['start'];
        $name = empty($data['name'])?'':$data['name'];
        $end = empty($data['end'])?'':$data['end'];
        if($start){
            $condition[] = ['create_time','egt' ,strtotime($start)];
        }
        if($end){
            $condition[] = ['create_time','lt' ,strtotime($end)];
        }
        if($name){
            $condition[]= ['name','like', "%{$name}%"];
        }
        $order = 'order desc';
        $navModel = new NavModel();
        $navList = $navModel->getDataList([],$order,'*',10);
        $this->assign('navList',$navList);
        $this->assign('start',$start);
        $this->assign('end',$end);
        $this->assign('name',$name);
        return $this->fetch();
    }

    public function add(Request $request)
    {
        if($request->isPost()){
            (new NavValidate())->goCheck($request);
            $data = $request->post();
            $navModel = new NavModel();
            $data['path'] = $this->getNavPath($data['skip_id']);
            $data['miniapp_id'] = $this->miniapp_id;
            $navModel->allowField(true)->save($data);
            $this->log('添加导航 名称:'.$data['name']);
            $this->clearCache('siteData');
            $this->success('添加成功','',['status' => 1]);
        }else{
            $categoryModel = new CategoryModel();
            $categoryList = $categoryModel->getDataList([]);
            $this->assign('categoryList',$categoryList);
            return $this->fetch();
        }
    }

    public function edit(Request $request)
    {
        $id = $request->param('id');
        $navModel = new NavModel();
        if($request->isPost()){
            (new NavValidate())->goCheck($request);
            $data = $request->post();
            $data['path'] = $this->getNavPath($data['skip_id']);
            $data['miniapp_id'] = $this->miniapp_id;
            $navModel->allowField(true)->update($data,['id' => $id,'miniapp_id' => $this->miniapp_id]);
            $this->log('编辑导航 名称:'.$data['name']);
            $this->clearCache('siteData');
            $this->success('保存成功','',['status' => 1]);
        }else{
            $navDetail = $navModel->getDataDetail(['id' => $id]);
            $categoryModel = new CategoryModel();
            $categoryList = $categoryModel->getDataList();
            $this->assign('categoryList',$categoryList);
            $this->assign('navDetail',$navDetail);
            return $this->fetch();
        }
    }

    // 获取导航路径
    public function getNavPath($data)
    {
        $path= '/pages/tabBarTwo/tabBarTwo';
        if($data == -1){
            $path = '/pages/index/index';
        }
        if($data == -2){
            $path = '/pages/tabBarThree/tabBarThree';
        }        
        if($data == -3){
            $path = '/pages/tabBarFour/tabBarFour';
        }
        if($data == -4){
            $path = '/pages/tabBarFive/tabBarFive';
        }
        return $path;
    }

    // 更新字段值
    public function field(Request $request)
    {
        $data = $request->param();
        $navModel = new NavModel();
        $result = $navModel->updateField($data);
        $this->clearCache('siteData');
        $this->success($result['msg'],'',['status' => $result['status']]);
    }

    // 删除
    public function delete(Request $request)
    {
        $id = $request->param('id');
        $navModel = new NavModel();
        $result = $navModel->deleteData($id);
        $this->clearCache('siteData');
        $this->success($result);
    }


}
