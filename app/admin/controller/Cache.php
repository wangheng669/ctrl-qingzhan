<?php

namespace app\admin\controller;

use think\Request;
use app\common\controller\Base;

class Cache extends Base
{

    public function index(Request $request)
    {
        if($request->isPost()){
            $data = $request->post('data');
            if($data){
                foreach($data as $cache){
                    $this->clearCache($cache);
                }
                $this->success('清除成功','',['status' => 1]);
            }
            $this->success('清除失败','',['status' => 2]);
        }else{
            return $this->fetch();
        }
    }

}
