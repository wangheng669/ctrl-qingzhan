<?php

namespace app\admin\controller;

use app\common\controller\Base;
use app\cms\model\Article as ArticleModel;
use app\cms\model\Banner as BannerModel;
use app\cms\model\Notice as NoticeModel;
use app\cms\model\Category as CategoryModel;
use app\cms\model\Adv as AdvModel;
use app\cms\model\Form as FormModel;
use app\miniapp\model\Notify as NotifyModel;
use app\admin\service\MenuService;
use app\common\service\UserService;

class Index extends Base
{
    public function index()
    {
        $menuTree = $this->getCache('menuTree');
        if(!$menuTree){
            // 获取菜单
            $menuService = new MenuService();
            $userService = new UserService();
            $roles = $userService->getUserRole();
            $condition[] = ['id','in',$roles];
            $condition[] = ['status','eq',1];
            $menuTree = $menuService->getMenuTree($condition);
            $this->setCache('menuTree',$menuTree);
        }
        $this->assign('menuTree',$menuTree);
        $notifyModel = new NotifyModel();
        $notifyCount = $notifyModel->where(['status' => 0,'miniapp_id' => $this->miniapp_id])->count();
        $this->assign('notifyCount',$notifyCount);
        return $this->fetch();
    }

    public function welcome()
    {
        $condition = ['miniapp_id' => session('miniapp_id'),'delete_time' => null];
        $bannerCount = BannerModel::where($condition)->count();
        $noticeCount = NoticeModel::where($condition)->count();
        $articleCount = ArticleModel::where($condition)->count();
        $categoryCount = CategoryModel::where($condition)->count();
        $advCount = AdvModel::where($condition)->count();
        $formCount = FormModel::where($condition)->count();
        $this->assign('bannerCount',$bannerCount);
        $this->assign('noticeCount',$noticeCount);
        $this->assign('articleCount',$articleCount);
        $this->assign('categoryCount',$categoryCount);
        $this->assign('advCount',$advCount);
        $this->assign('formCount',$formCount);
        return $this->fetch();
    }

}
