<?php

namespace app\admin\controller;

use think\Request;
use app\common\controller\Base;
use app\admin\model\Setting as SettingModel;

class Setting extends Base
{
    

    /**
     * 初始操作
     * @author WangHeng
     */
    protected function initialize()
    {
        parent::initialize();
        // 获取模板配置
        $condition[] = ['type','in','bg,logo'];
        $this->getTemplateConfig($condition,1);
    }

    /**
     * 基本信息
     */
    public function index(Request $request)
    {
        $settingModel = new SettingModel();
        if($request->isPost()){
            $siteInfo = $request->param('site_info/a');
            $siteStyle = $request->param('site_style/a');
            $settingModel->setSettingValue('site_info',$siteInfo);
            $settingModel->setSettingValue('site_style',$siteStyle);
            $this->log('修改基础信息');
            $this->clearCache('siteData');
            $this->success('保存成功','',['status' => 1,'is_load' => 2]);
            
        }else{
            $siteInfo = $settingModel->getSettingValue('site_info');
            $siteStyle = $settingModel->getSettingValue('site_style');
            $this->assign('siteInfo',$siteInfo);
            $this->assign('siteStyle',$siteStyle);
            return $this->fetch();
        }
    }

    /**
     * 选择地图
     */
    public function map(Request $request)
    {
        $lng = $request->param('lng');
        $lat = $request->param('lat');
        $this->assign('lng',$lng);
        $this->assign('lat',$lat);
        return $this->fetch();
    }

}
