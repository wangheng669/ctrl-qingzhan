<?php
/*
 * @Description: 回收站
 * @Author: WangHeng
 * @Date: 2019-08-26 23:06:34
 * @LastEditTime: 2019-08-26 23:10:33
 */

namespace app\admin\controller;

use app\common\controller\Base;
use app\cms\controller\Article as ArticleModel;

class Receylebin extends Base
{

    public function index()
    {
        // 获取被删除文章
        $articleModel = new ArticleModel();
        $articleModel->getDeleteData();
    }

}