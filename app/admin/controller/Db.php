<?php
/*
 * @Description: 数据处理
 * @Author: WangHeng
 * @Date: 2019-07-11 22:50:51
 * @LastEditTime: 2019-09-09 23:03:44
 */

namespace app\admin\controller;

use app\admin\service\DbService;
use think\Request;
use app\common\controller\Base;
use app\admin\model\Db as DbModel;

class Db extends Base
{
    public function index()
    {
        $dbModel = new DbModel();
        $dbList = $dbModel->getDataList([],'id desc','*',10);
        $this->assign('dbList',$dbList);
        return $this->fetch();
    }

    // 备份数据
    public function backup(Request $request)
    {
        if($request->isPost()){
            $name = $request->post('name');
            $dbService = new DbService();
            $dbService->backupData($name);
            $this->log('备份数据 名称:'.$name);
            $this->success('备份成功','',['status' => 1]);
        }else{
            return $this->fetch();
        }
        
    }

    // 导入数据
    public function import(Request $request)
    {
        $id = $request->param('id');
        $dbService = new DbService();
        $dbService->importData($id);
        $this->log('导入数据 名称:'.date('Y-m-d H:i',time()));
        $this->success('导入成功','',['status' => 1]);
    }
    
    // 删除
    public function delete(Request $request)
    {
        $id = $request->param('id');
        $dbModel = new DbModel();
        $result = $dbModel->deleteData($id);
        $this->success($result);
    }

    // 数据复制
    public function copy(Request $request)
    {
        if($request->isPost()){
            $miniapp_id = $request->param('id');
            $dbService = new DbService();
            $dbService->copyData($miniapp_id);
            $this->log('导入数据 ID:'.$miniapp_id);
            $this->success('复制成功','',['status' => 1]);
        }else{
            $xcxList = $this->getPlatformData('miniapp.common.xcxlist',[]);
            $this->assign('xcxList',$xcxList);
            return $this->fetch();
        }
    }

}
