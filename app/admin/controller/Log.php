<?php

namespace app\admin\controller;

use think\Request;
use app\common\controller\Base;
use app\admin\model\Log as LogModel;
use app\user\model\User as UserModel;

class Log extends Base
{
    
    public function index(Request $request)
    {
        $data = $request->param();
        $condition = [];
        $start = empty($data['start'])?'':$data['start'];
        $user_id = empty($data['user_id'])?'':$data['user_id'];
        $end = empty($data['end'])?'':$data['end'];
        if($start){
            $condition[] = ['insert_time','egt' ,strtotime($start)];
        }
        if($end){
            $condition[] = ['insert_time','lt' ,strtotime($end)];
        }
        if($user_id){
            $condition[]= ['user_id','eq',$user_id];
        }
        $logModel = new LogModel();
        $userModel = new UserModel();
        $logList = $logModel->getLogList($condition);
        $userList = $userModel->getDataList([],'id desc');
        $this->assign('logList',$logList);
        $this->assign('userList',$userList);
        $this->assign('user_id',$user_id);
        $this->assign('start',$start);
        $this->assign('end',$end);
        return $this->fetch();
    }
}
