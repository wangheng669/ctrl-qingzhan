<?php
/*
 * @Description: 菜单管理
 * @Author: WangHeng
 * @Date: 2019-08-02 21:34:14
 * @LastEditTime: 2019-09-18 14:48:02
 */

namespace app\admin\controller;

use think\Request;
use app\common\controller\Base;
use app\admin\model\Menu as MenuModel;
use app\admin\validate\MenuValidate;
use app\admin\service\MenuService;

class Menu extends Base
{
    
    public function index()
    {
        // 获取菜单列表
        $menuModel = new MenuModel();
        $menuList = $menuModel->getDataList([],'path asc','','',0);
        $this->assign('menuList',$menuList);
        return $this->fetch();
    }


    public function add(Request $request)
    {
        if($request->isPost()){
            (new MenuValidate())->goCheck($request);
            $data = $request->post();
            $menuService = new MenuService();
            $menuService->addMenu($data);
            $this->log('添加菜单 名称:'.$data['name']);
            $this->clearCache('menuTree');
            $this->success('添加成功','',['status' => 1]);
        }else{
            $menuModel = new MenuModel();
            $menuList = $menuModel->getDataList([],'path asc','','',0);
            $this->assign('menuList',$menuList);
            return $this->fetch();
        }
    }
    
    public function edit(Request $request)
    {
        $id = $request->param('id');
        if($request->isPost()){
            (new MenuValidate())->goCheck($request);
            $data = $request->post();
            $menuService = new MenuService();
            $menuService->editMenu($data,$id);
            $this->log('编辑菜单 名称:'.$data['name']);
            $this->clearCache('menuTree');
            $this->success('保存成功','',['status' => 1]);
        }else{
            $menuModel = new MenuModel();
            $menuDetail = $menuModel->getDataDetail(['id' => $id],'*',0);
            $menu_condition[] = ['id','neq',$id];
            $menuList = $menuModel->getDataList($menu_condition,'path asc','','',0);
            $this->assign('menuList',$menuList);
            $this->assign('menuDetail',$menuDetail);
            return $this->fetch();
        }
    }

    // 删除
    public function delete(Request $request)
    {
        $id = $request->param('id');
        $menuModel = new MenuModel();
        $this->clearCache('menuTree');
        $result = $menuModel->deleteData($id,0);
        $this->success($result);
    }

    // 更新字段值
    public function field(Request $request)
    {
        $data = $request->param();
        $menuModel = new MenuModel();
        $this->clearCache('menuTree');
        $result = $menuModel->updateField($data,0);
        $this->success($result['msg'],'',['status' => $result['status']]);
    }

}
