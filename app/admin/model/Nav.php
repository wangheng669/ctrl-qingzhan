<?php

namespace app\admin\model;

use app\common\model\BaseModel;

class Nav extends BaseModel
{
    // 获取导航列表
    public function getNavList($condition,$order,$field='*',$size)
    {
        return $this->where($condition)->order($order)->field($field)->paginate($size);
    }

    // 获取导航信息
    public function getNavDetail($condition,$field='*')
    {
        return $this->where($condition)->field($field)->find();
    }
    
}
