<?php

namespace app\admin\model;

use app\common\model\BaseModel;

class Setting extends BaseModel
{
    // 获取配置值
    public function getSettingValue($name)
    {
        $result = $this->where(['name' => $name,'miniapp_id' => session('miniapp_id')])->value('value');
        return json_decode($result,true);
    }

    // 设置配置值
    public function setSettingValue($name,$value)
    {
        $this->where(['name' => $name,'miniapp_id' => session('miniapp_id')])->update(['value' => json_encode($value)]);
        return true;
    }

}
