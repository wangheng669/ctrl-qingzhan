<?php

namespace app\admin\model;

use app\common\model\BaseModel;

class Menu extends BaseModel
{
    // 获取菜单详情
    public function getMenuDetail($condition,$field='*')
    {
        return $this->where($condition)->field($field)->find();
    }

}
