<?php

namespace app\admin\model;

use app\common\model\BaseModel;

class Log extends BaseModel
{
    // 获取日志列表
    public function getLogList($condition)
    {
        return $this->where($condition)->where(['miniapp_id' => session('miniapp_id')])->with('user')->paginate();
    }

    // 关联用户表
    public function user()
    {
        return $this->belongsTo('\app\user\model\User');
    }

    // 时间转换
    public function getInsertTimeAttr($value)
    {
        return date('Y-m-d H:i:s',$value);
    }
}
