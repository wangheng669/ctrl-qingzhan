<?php

use think\Db;

function category_line($value)
{
    return str_repeat('&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',$value);
}

// 权限id转换
function authMenu($value)
{
    $result = Db::name('menu')->where('id','in',$value)->column('name');
    $result = implode(',',$result);
    return $result;
}

// 用户角色转换
function userMenu($value)
{
    $result = Db::name('role')->where('id','in',$value)->column('name');
    $result = implode(',',$result);
    return $result;
}
