<?php
/*
 * @Description: 参数异常
 * @Author: WangHeng
 * @Date: 2019-07-12 23:53:45
 */

namespace app\lib\exception;

class ParamterException extends BaseException
{

    public $code = 200;

    public $msg = '参数异常';

    public $errcode = 40000;
    
    public $status = 7;

}
