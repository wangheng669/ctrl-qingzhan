<?php

namespace app\lib\exception;

use \Exception;
use think\exception\Handle;
use think\facade\Log;

class ExceptionHandle extends Handle{

    public $msg;
    public $code;
    public $errcode;
    public $status;

    /* 重写异常捕获方法 */
    public function render(Exception $e)
    {
        if($e instanceof BaseException){
            $this->msg = $e->msg;
            $this->code = $e->code;
            $this->errcode = $e->errcode;
            $this->status = $e->status;
            $result = [
                'code' => $this->code,
                'errcode' => $this->errcode,
                'msg' => $this->msg,
                'status' => $this->status,
            ];    
            return json($result,$this->code);
        }else{
            if(config('app_debug')){
                return parent::render($e);
            }
            // 日志记录
            $this->recordErrorLog($e);
            header("Location:".'/404');
        }
    }

    /* 记录错误日志 */
    public function recordErrorLog(Exception $e)
    {
        Log::record($e->getMessage());
        Log::save();
    }

}