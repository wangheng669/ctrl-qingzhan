<?php
/*
 * @Description: 接口异常
 * @Author: WangHeng
 * @Date: 2019-07-14 23:00:42
 */

namespace app\lib\exception;

class ApiException extends BaseException
{

    public $code = 200;

    public $msg = '接口异常';
    
    public $errcode = 40000;
    
    public $status = 2;

}
