<?php
/*
 * @Description: 公共异常
 * @Author: WangHeng
 * @Date: 2019-07-12 23:53:45
 */

namespace app\lib\exception;

use think\Exception;

class BaseException extends Exception
{

    public $code = 200;

    public $msg = '服务器内部错误';

    public $errcode = 999;

    public $status = 2;

    public function __construct($params=[])
    {
        if(array_key_exists('msg',$params)){
            $this->msg = $params['msg'];
        }
        if(array_key_exists('code',$params)){
            $this->code = $params['code'];
        }
        if(array_key_exists('errCode',$params)){
            $this->errcode = $params['errCode'];
        }
        if(array_key_exists('status',$params)){
            $this->status = $params['status'];
        }
    }


}
