<?php
/*
 * @Description: 用户服务层
 * @Author: WangHeng
 * @Date: 2019-07-14 22:27:39
 */

namespace app\common\service;

use app\user\model\User as UserModel;
use app\user\model\Role as RoleModel;
use app\admin\model\Menu as MenuModel;
use app\lib\exception\ApiException;

class UserService
{

    /**
     * 用户登录校验
     * @author WangHeng
     */
    public function loginVerify($request)
    {
        $login = $request->post('login');
        $password = $request->post('password');
        $userModel = new UserModel();
        $userInfo = $userModel->getDataDetail(['login' => $login],'id,password,miniapp_id',0);
        // 比较密码
        if(md5($password) != $userInfo['password']){
            throw new ApiException([
                'msg' => '密码错误',
            ]);
        }
        // 存储小程序id
        session('miniapp_id',$userInfo['miniapp_id']);
        session('USER_ID',$userInfo['id']);
        return true;
    }

    /**
     * 校验是否登录
     * @author WangHeng
     */
    public function isLogin()
    {
        return empty(session('USER_ID'))?false:true;
    }

    /**
     * 校验规则
     */
    public function getUserRole()
    {
        $roleModel = new RoleModel();
        $userModel = new UserModel();
        $userInfo = $userModel->getDataDetail(['id' => session('USER_ID')]);
        $roleList = $roleModel->field('auth')->where(['delete_time' => 0, 'id' => ['in',$userInfo['role']]])->select()->withAttr('auth',function($value,$data){
            return explode(',',$value);
        });
        $roles = [];
        foreach($roleList as $role){
            $roles = array_merge($roles,$role['auth']);
        }
        $roles = implode(',',array_unique($roles));
        return $roles;
    }
    
    /**
     * 校验规则
     */
    public function getUserMenu()
    {
        $roles = $this->getUserRole();
        $menuModel = new MenuModel();
        $condition[] = ['delete_time','eq',0];
        $condition[] = ['id','in',$roles];
        $menuList = $menuModel->where($condition)->column('method');
        return (array)$menuList;
    }

}