<?php
/*
 * @Description: 公共校验类
 * @Author: WangHeng
 * @Date: 2019-07-12 23:51:10
 */

namespace app\common\validate;

use think\Validate;
use app\lib\exception\ParamterException;
use think\Request;

class BaseValidate extends Validate
{
    /* 公共校验方法 */
    public function goCheck(Request $request)
    {
        $data = $request->param();
        $result = $this->check($data);
        if(!$result){
            throw new ParamterException([
                'msg' => $this->getError(),
                'code' => 200,
                'status' => 7,
            ]);
        }
        return true;
    }
}
