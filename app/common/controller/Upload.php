<?php

namespace app\common\controller;

use think\Request;

class Upload extends Base
{
   
    public function index(Request $request)
    {
        $type = $request->param('type');
        $uploadImg = $request->file('uploadImg');
        $info = $uploadImg->move( '../public/uploads/u'.$this->miniapp_id);
        $save_path = $info->getRealPath();
        thumb_img($save_path,$type);
        if($info){
            $data = [
                'src'  => 'https://'.$_SERVER['HTTP_HOST'].'/uploads/u'.$this->miniapp_id.'/'.$info->getSaveName(),
            ];
            return json($data);
        }else{
            return $uploadImg->getError();
        }
    }
    
    public function upload(Request $request)
    {
        $uploadImg = $request->file('upfile');
        $info = $uploadImg->move( '../public/uploads/u'.$this->miniapp_id);
        $save_path = $info->getRealPath();
        thumb_img($save_path,'ueditor');
        if($info){
            $data = [
                "state" => 'SUCCESS',
                "title" => $info->getSaveName(),
                "original" => $info->getSaveName(),
                "type" => $info->getMime(),
                "size" => $info->getSize(),
                'url'  => 'https://'.$_SERVER['HTTP_HOST'].'/uploads/u'.$this->miniapp_id.'/'.$info->getSaveName(),
            ];
            return json_encode($data);
        }else{
            return $uploadImg->getError();
        }
    }

    public function getRemoteImg(Request $request)
    {

        // 保存路径
        $savePath = 'uploads/u'.$this->miniapp_id.'/'.date('Ymd').'/';
        $data = $request->param();
        $source = $data['source'];

        $imgArr = [];

        foreach($source as $imgUrl){
            ob_start();
            $context = stream_context_create(
                [
                    'http' => [
                        'follow_location' => false // don't follow redirects
                    ]
                ]
            );
            //请确保php.ini中的fopen wrappers已经激活
            readfile($imgUrl, false, $context);
            $img = ob_get_contents();
            ob_end_clean();
            //创建保存位置
            if(!is_dir($savePath)){
                mkdir($savePath, 0777, true);
            }
            // 文件名称
            $filename = md5(time()).strrchr($imgUrl, '.');
            // 临时目录
            $tmpName = $savePath.$filename;
            // 写入图片
            $newImg = fopen($tmpName,"w"); //打开文件准备写入
            fwrite($newImg,$img); //写入二进制流到文件
            fclose($newImg); //关闭文件
            // 组合路径
            $filePath = $savePath.$filename;
            thumb_img($filePath,'ueditor');
            $return_img['state'] = 'SUCCESS';
            $return_img['url']   = 'https://'.$_SERVER['HTTP_HOST'].'/'.$filePath;
            $return_img['source']   = htmlspecialchars_decode($imgUrl);
            array_push($imgArr, $return_img);
        }
        return json_encode([
            'state' => 'SUCCESS',
            'list'  => $imgArr
        ]);
    }

}
