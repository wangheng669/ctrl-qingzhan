<?php

namespace app\common\controller;

use think\Request;
use think\facade\App;
use think\exception\HttpResponseException;
use think\Response;

class Ueditor extends Base
{
   
    public function upload(Request $request)
    {
        $upload = new Upload();
        $config_text    = preg_replace("/\/\*[\s\S]+?\*\//", "", file_get_contents(App::getRootPath()."public//static/ueditor/php/config.json"));
        $action = $request->param('action');
        switch ($action) {
            case 'config':
                $result = $config_text;
                break;
            /* 上传图片 */
            case 'uploadimage':
                $result = $upload->upload($request);
                break;
            /* 抓取远程文件 */
            case 'catchimage':
                $result = $upload->getRemoteImg($request);
                break;
            default:
                $result = json_encode(array(
                    'state'=> '请求地址出错'
                ));
                break;
        }
        /* 输出结果 */
        if (isset($_GET["callback"])) {
            if (preg_match("/^[\w_]+$/", $_GET["callback"])) {
                echo htmlspecialchars($_GET["callback"]) . '(' . $result . ')';
            } else {
                echo json_encode(array(
                    'state'=> 'callback参数不合法'
                ));
            }
        } else {
            $response = Response::create(json_decode($result,true),'json');
            throw new HttpResponseException($response);
        }
    }


}
