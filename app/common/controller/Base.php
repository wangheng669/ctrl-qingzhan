<?php
/*
 * @Description: 基类控制器
 * @Author: WangHeng
 * @Date: 2019-07-12 22:59:42
 */

namespace app\common\controller;

use think\Controller;
use app\cms\model\Template;
use app\admin\model\Log as LogModel;
use app\lib\exception\ApiException;
use app\user\model\User as UserModel;
use app\admin\model\Menu as MenuModel;
use think\facade\Request;
use think\facade\Cache;

class Base extends Controller
{

    
    public $miniapp_id;

    public $platform_url = 'https://control.ctrlyun.com/rest';


    /**
     * 公共导航方法
     * @author WangHeng
     */
    protected $beforeActionList = [
        'getTab' => ['only'=>'index'],
    ];

    /**
     * 获取当前页面导航
     */
    public function getTab()
    {
        $method = $this->getMethodPath();
        $menuModel = new MenuModel();
        $menuDetail = $menuModel->getDataDetail(['method' => $method],'path',0);
        $tabList = $menuModel->getDataList([['id','in',$menuDetail['path']]],'','name','',0);
        $this->assign('tabList',$tabList); 
    }

    /**
     * 初始操作
     * @author WangHeng
     */
    protected function initialize()
    {
        $this->miniapp_id = session('miniapp_id');
        $user_id = session('USER_ID');
        if (empty($user_id)) {
            $this->error("您还没有登录！", url("admin/login/index"));
        }
        $this->checkAuth();
        $userModel = new UserModel();
        $userInfo = $userModel->getDataDetail(['id' => $user_id]);
        $this->assign('userInfo',$userInfo);
        return true;
    }
    
    /**
     * 获取模板配置
     * @author WangHeng
     */
    protected function getTemplateConfig($condition,$is_system)
    {
        $xcxExcerpt = $this->getXcxExcerpt();
        session('template_id',$xcxExcerpt['template_id']);
        $template = new Template();
        $configList = $template->getTemplateConfig($condition,$is_system);
        $this->assign('configList',$configList);
    }
    
    /**
     * 日志记录
     * @author WangHeng
     */
    public function log($content)
    {
        $logModel = new LogModel();
        $logModel->insert([
            'user_id' => session('USER_ID'),
            'content' => $content,
            'miniapp_id' => session('miniapp_id'),
            'insert_time' => time(),
        ]);
        return true;
    }

    /**
     * 调用平台接口
     * @author WangHeng
     */
    public function getPlatformData($method,$param)
    {
        $data = [
            'method' => 'ctrl.xcx.subproject.'.$method,
            'timestamp' => date('Y-m-d H:i:s',time()),
            'appkey' => '190429035259100000005',
            'code' => $this->getCode(),
        ];
        $param = array_merge($data,$param);
        $result = curl_post($this->platform_url,$param);
        $platformResult = json_decode($result,true);
        if(isset($platformResult['code']) && $platformResult['code'] == 200){
            return $platformResult['data'];
        }else{
            $platformResult['code'] = 200;
            file_put_contents('1.txt',$result);
            throw new ApiException($platformResult);
        }
    }

    /**
     * 获取平台code
     * @author WangHeng
     */
    public function getCode(){
        // 获取小程序appkey
        $xcxExcerpt = $this->getXcxExcerpt();
        $originStr='190429035259100000005|'.$this->miniapp_id.'|'.$xcxExcerpt['appkey'].'|'.$_SERVER['REQUEST_TIME'];
        return authcode($originStr);
    }

    /**
     * 获取小程序概况
     * @author WangHeng
     */
    public function getXcxExcerpt()
    {
        $xcxExcerpt = $this->getCache('xcxExcerpt');
        if(!$xcxExcerpt){
            $data = [
                'method' => 'ctrl.xcx.subproject.miniapp.common.appkey',
                'timestamp' => date('Y-m-d H:i:s',time()),
                'appkey' => '190429035259100000005',
                'miniapp_id' => $this->miniapp_id,
            ];
            $result = json_decode(curl_post($this->platform_url,$data),true);
            if($result['code'] == 200){
                $xcxExcerpt = $result['data'];
                $this->setCache('xcxExcerpt',$xcxExcerpt);
            }else{
                // 处理报错
                dump($result);
            }
        }
        return $xcxExcerpt;
    }


    // 校验权限
    public function checkAuth()
    {
        $method = $this->getMethodPath();
        $menuList = session('menuList');
        // if(!in_array($method,$menuList) && $method != "/"){
        //     $this->error('无权限访问','/admin/index/index');
        // }
    }

    // 校验小程序是否授权并返回小程序信息 - 待完善功能
    public function checkXcxEmpower()
    {
        $xcxInfo = $this->getCache('xcxInfo');
        if(!$xcxInfo){
            $xcxInfo = $this->getPlatformData('miniapp.baidu.excerpt',[]);
            $this->setCache('xcxInfo',$xcxInfo);
        }
        // 如果没有授权
        if($xcxInfo['detail']['empower_status'] == 0){
            $this->empower();
        }
        return $xcxInfo;
    }

    // 授权页面 - 需要修改
    public function empower()
    {
        $str = '190429035259100000001|2|'.$this->miniapp_id.'|'.time();
        $code = urlencode(authcode($str));
        $url = 'https://control.ctrlyun.com/miniapp/empower/baidu?code='.$code;
        header("Location:".$url);
    }

    // 微信授权页面 - 需要修改
    public function wxEmpower()
    {
        $str = '190429035259100000001|2|'.$this->miniapp_id.'|'.time();
        $code = urlencode(authcode($str));
        $url = 'https://control.ctrlyun.com/miniapp/empower/wechat?code='.$code;
        header("Location:".$url);
    }
    // 获取当前方法路径
    private function getMethodPath()
    {
        $module = Request::module();
        $controller = Request::controller();
        $action = Request::action();
        $method = '/'.strtolower($module.'/'.$controller.'/'.$action);
        return $method;
    }

    
    // 回收站
    public function recyclebin()
    {
        $controller = Request::controller();
        $condition[] = ['delete_time','neq',0];
        $condition[] = ['miniapp_id','eq',$this->miniapp_id];
        $list = model($controller)->where($condition)->paginate(10,false, [
            'query' => request()->param(),
        ]);
        $this->assign('list',$list);
        return $this->fetch();
    }

    // 回收站清空
    public function recyclebinClear()
    {
        $id = Request::param('id');
        $controller = Request::controller();
        $condition[] = ['delete_time','neq',0];
        $condition[] = ['miniapp_id','eq',$this->miniapp_id];
        model($controller)->where($condition)->delete($id);
        $this->success('删除成功','',['status' => 1]);
    }

    // 回收站还原
    public function recyclebinRestore()
    {
        $id = Request::param('id');
        $controller = Request::controller();
        $condition[] = ['delete_time','neq',0];
        $condition[] = ['id','eq',$id];
        $condition[] = ['miniapp_id','eq',$this->miniapp_id];
        model($controller)->where($condition)->update(['delete_time' => 0]);
        $this->success('还原成功','',['status' => 1]);
    }

    // 清除缓存
    public function clearCache($name)
    {
        Cache::store('redis')->set($name.$this->miniapp_id,null);
    }

     // 获取redis缓存
     public function getCache($name)
     {
        $data = Cache::store('redis')->get($name.$this->miniapp_id);
        return $data;
     }
     
     // 设置redis缓存
     public function setCache($name,$data)
     {
        Cache::store('redis')->set($name.$this->miniapp_id,$data);
     }


     // 授权回调
     public function callback()
     {
         $this->assign('msg','授权成功');
         return $this->fetch();
     }

}
