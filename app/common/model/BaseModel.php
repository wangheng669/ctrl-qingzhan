<?php

namespace app\common\model;

use think\Model;

class BaseModel extends Model
{

    // 获取列表
    public function getDataList($condition = [],$order = 'id desc',$field = '*',$size = '',$isCheck = 1,$limit = '')
    {
        if($isCheck){
            $map = ['miniapp_id' => session('miniapp_id'),'delete_time' => 0];
        }else{
            $map = ['delete_time' => 0];
        }
        if($size){
            return $this->where($condition)->where($map)->field($field)->order($order)->paginate($size,false, [
                'query' => request()->param(),
            ]);
        }else{
            return $this->where($condition)->where($map)->field($field)->order($order)->limit($limit)->select();
        }
    }

    // 获取详情
    public function getDataDetail($condition=[],$field='*',$isCheck = 1)
    {
        if($isCheck){
            $map = ['miniapp_id' => session('miniapp_id'),'delete_time' => 0];
        }else{
            $map = ['delete_time' => 0];
        }
        return $this->where($condition)->where($map)->field($field)->find();
    }

    
    // 更新字段
    public function updateField($data,$isCheck=1)
    {
        $fieldResult = [
            'status' => 0,
            'msg' => '更新失败',
        ];
        if(!$data['value'] < 0){
            return $fieldResult;
        }
        $map = [];
        if($isCheck){
            $map = ['miniapp_id' => session('miniapp_id')];
        }
        $result = $this->where(['id' => $data['id']])->where($map)->setField([$data['field'] => $data['value']]);
        if(!$result){
            return $fieldResult;
        }
        return [
            'status' => 1,
            'msg' => '更新成功',
        ];
    }

    // 公共删除方法
    public function deleteData($id,$isCheck=1)
    {
        $map = [];
        if($isCheck){
            $map = ['miniapp_id' => session('miniapp_id')];
        }
        $result = $this->useSoftDelete('delete_time',time())->where($map)->delete($id);
        if(!$result){
            return '删除失败';
        }
        return '删除成功';
    }

}
