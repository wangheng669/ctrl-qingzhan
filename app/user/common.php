<?php

use think\Db;

// 权限id转换
function authMenu($value)
{
    $result = Db::name('menu')->where('id','in',$value)->column('name');
    $result = implode(',',$result);
    return $result;
}

// 用户角色转换
function userMenu($value)
{
    $result = Db::name('role')->where('id','in',$value)->column('name');
    $result = implode(',',$result);
    return $result;
}