<?php
/*
 * @Description: 角色验证
 * @Author: WangHeng
 * @LastEditTime: 2019-08-04 17:46:28
 */
namespace app\user\validate;

use app\common\validate\BaseValidate;

class RoleValidate extends BaseValidate
{
    
	protected $rule = [
        'name' => 'require',
        'auth' => 'require',
        'status' => 'require',
    ];
    
    
    protected $message = [
        'name.require' => '请填写角色名称',
        'auth.require' => '请填写角色权限',
        'status.require' => '请选择角色状态',
    ];
}
