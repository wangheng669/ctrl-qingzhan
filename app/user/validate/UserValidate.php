<?php
/*
 * @Description: 用户验证
 * @Author: WangHeng
 * @LastEditTime: 2019-08-04 17:46:32
 */
namespace app\user\validate;

use app\common\validate\BaseValidate;

class UserValidate extends BaseValidate
{
    
	protected $rule = [
        'nick_name' => 'require',
        'tel' => 'require',
        'login' => 'require',
        'password' => 'require',
        'repassword' => 'require|confirm:password',
        'role' => 'require',
        'status' => 'require',
    ];
    
    
    protected $message = [
        'nick_name.require' => '请填写昵称',
        'tel.require' => '请选择手机号',
        'login.require' => '请选择登录名',
        'password.require' => '请填写密码',
        'repassword.require' => '请填写密码',
        'repassword.confirm' => '密码不一致',
        'role.require' => '请选择角色',
        'status.require' => '请填写状态',
    ];
}
