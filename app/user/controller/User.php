<?php
/*
 * @Description: 管理员
 * @Author: WangHeng
 * @Date: 2019-07-11 23:11:45
 */

namespace app\user\controller;

use think\Request;
use app\common\controller\Base;
use app\user\model\User as UserModel;
use app\user\model\Role as RoleModel;
use app\user\validate\UserValidate;

class User extends Base
{
    public function index(Request $request)
    {
        $data = $request->param();
        $condition = [];
        $start = empty($data['start'])?'':$data['start'];
        $nick_name = empty($data['nick_name'])?'':$data['nick_name'];
        $end = empty($data['end'])?'':$data['end'];
        if($start){
            $condition[] = ['create_time','egt' ,strtotime($start)];
        }
        if($end){
            $condition[] = ['create_time','lt' ,strtotime($end)];
        }
        if($nick_name){
            $condition[]= ['nick_name','like', "%{$nick_name}%"];
        }
        $order = 'id desc';
        $userModel = new UserModel();
        $userList = $userModel->getDataList($condition,$order,'*',10);
        $this->assign('userList',$userList);
        $this->assign('nick_name',$nick_name);
        $this->assign('start',$start);
        $this->assign('end',$end);
        return $this->fetch();
    }

    // 添加
    public function add(Request $request,UserModel $userModel)
    {
        if($request->isPost()){
            (new UserValidate())->goCheck($request);
            // 获取当前用户的小程序名称
            $data = $request->post();
            $userDetail = $userModel->getDataDetail(['id' => session('USER_ID')],'miniapp_name');
            $data['role'] = implode(',',$data['role']);
            $data['miniapp_id'] = session('miniapp_id');
            $data['password'] = $this->passwordHandle($data['password']);
            $data['miniapp_name'] = $userDetail['miniapp_name'];
            $userModel->allowField(true)->save($data);
            $this->log('添加用户 名称:'.$data['nick_name']);
            $this->success('添加成功','',['status' => 1]);
        }else{
            $roleModel = new RoleModel();
            $roleList = $roleModel->getDataList([],'id desc');
            $this->assign('roleList',$roleList);
            return $this->fetch();
        }
    }
    
    // 编辑
    public function edit(Request $request)
    {
        $id = $request->param('id');
        $userModel = new UserModel();
        if($request->isPost()){
            (new UserValidate())->goCheck($request);
            $data = $request->post();
            $data['role'] = implode(',',$data['role']);
            $data['miniapp_id'] = session('miniapp_id');
            $data['password'] = $this->passwordHandle($data['password']);
            $userModel->allowField(true)->update($data,['id' => $id]);
            $this->log('编辑用户 名称:'.$data['nick_name']);
            $this->success('保存成功','',['status' => 1]);
        }else{
            $userInfo = $userModel->getDataDetail(['id' => $id]);
            $roleModel = new RoleModel();
            $roleList = $roleModel->getDataList([],'id desc');
            $this->assign('userInfo',$userInfo);
            $this->assign('roleList',$roleList);
            return $this->fetch();
        }
    }

    // 更新字段值
    public function field(Request $request)
    {
        $data = $request->param();
        $userModel = new UserModel();
        $result = $userModel->updateField($data);
        $this->success($result['msg'],'',['status' => $result['status']]);
    }

    // 删除
    public function delete(Request $request)
    {
        $id = $request->param('id');
        $userModel = new UserModel();
        $result = $userModel->deleteData($id);
        $this->success($result);
    }

    // 密码处理
    public function passwordHandle($password)
    {
        // 进行加盐
        $password = $password;
        return md5($password);
    }

}
