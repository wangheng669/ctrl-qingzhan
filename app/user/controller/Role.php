<?php
/*
 * @Description: 角色管理
 * @Author: WangHeng
 * @Date: 2019-07-11 23:11:34
 * @LastEditTime: 2019-11-05 17:11:59
 */

namespace app\user\controller;

use think\Request;
use app\common\controller\Base;
use app\admin\service\MenuService;
use app\common\service\UserService;
use app\user\validate\RoleValidate;
use app\user\model\Role as RoleModel;

class Role extends Base
{
    public function index()
    {
        $roleModel = new RoleModel();
        $roleList = $roleModel->getDataList([],'id desc');
        $this->assign('roleList',$roleList);
        return $this->fetch();
    }

    // 添加
    public function add(Request $request)
    {
        if($request->isPost()){
            (new RoleValidate())->goCheck($request);
            $data = $request->post();
            $data['auth'] = implode(',',$data['auth']);
            $data['miniapp_id'] = session('miniapp_id');
            $roleModel = new RoleModel();
            $roleModel->allowField(true)->save($data);
            $this->log('添加角色 名称:'.$data['name']);
            $this->success('添加成功','',['status' => 1]);
        }else{
            // 获取菜单
            $menuService = new MenuService();
            $menuTree = $menuService->getMenuTree([]);
            $this->assign('menuTree',$menuTree);
            return $this->fetch();
        }
    }
    
    // 编辑
    public function edit(Request $request)
    {
        $id = $request->param('id');
        $roleModel = new RoleModel();
        if($request->isPost()){
            (new RoleValidate())->goCheck($request);
            $data = $request->post();
            $data['auth'] = implode(',',$data['auth']);
            $roleModel->allowField(true)->update($data,['id' => $id]);
            $this->log('编辑角色 名称:'.$data['name']);
            $this->success('保存成功','',['status' => 1]);
        }else{
            $menuService = new MenuService();
            $menuTree = $menuService->getMenuTree([]);
            $roleDetail = $roleModel->getDataDetail(['id' => $id]);
            $this->assign('menuTree',$menuTree);
            $this->assign('roleDetail',$roleDetail);
            return $this->fetch();
        }
    }

    // 更新字段值
    public function field(Request $request)
    {
        $data = $request->param();
        $roleModel = new RoleModel();
        $result = $roleModel->updateField($data);
        $this->success($result['msg'],'',['status' => $result['status']]);
    }

    // 删除
    public function delete(Request $request)
    {
        $id = $request->param('id');
        $roleModel = new RoleModel();
        $result = $roleModel->deleteData($id);
        $this->success($result);
    }

}
