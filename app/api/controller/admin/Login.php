<?php

namespace app\api\controller\admin;

use app\user\model\User as UserModel;
use app\common\service\UserService;

class Login{

    public function index($code)
    {
        $cms_aesKey = base64_encode('V9gwxG2S9MH9aRn3uqeYF27rSkvbCPqw');
        $dataCoder = new \miniapp\AesEncryptUtil(1, $cms_aesKey);
        $result = $dataCoder->decrypt($code);
        $authData = authcode($result, 'DECODE');
        list($miniapp_id, $app_key, $type, $time) = explode('|', $authData);
        session('miniapp_id', $miniapp_id);
        // 查询是否存在该用户
        $userModel = new UserModel();
        $userDetail = $userModel->getDataDetail(['miniapp_id' => $miniapp_id , 'is_system' => 1]);
        if($userDetail){
            session('USER_ID',$userDetail['id']);
            $userService = new UserService();
            $menuList = $userService->getUserMenu();
            session('menuList',$menuList);
            header('Location:https://qingzhan.ctrlyun.com/');
        }else{
            exit('用户不存在');
        }
    }

}