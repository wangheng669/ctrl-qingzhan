<?php

namespace app\api\controller\admin;

use think\Request;
use app\admin\model\Template as TemplateModel;

class Template{

    public function index(Request $request)
    {
        $templateModel = new TemplateModel();
        $data = $request->post();
        $templateArr = json_decode($data['config'],true);
        $insertArr = [];
        foreach($templateArr as $k=>$v){
            $insertArr[$k] = $v;
            $insertArr[$k]['template_id'] = $data['template_id'];
        }
        // 先删除
        $templateModel->where(['template_id' => $data['template_id']])->delete();
        $templateModel->insertAll($insertArr);
        return json(['code' => 200, 'msg' => '成功']);
    }

}