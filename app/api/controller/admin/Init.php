<?php

namespace app\api\controller\admin;

use app\user\model\User as UserModel;
use think\Request;
use think\Db;

class Init{

    public function index($code,Request $request)
    {
        $initData = $request->post();
        $cms_aesKey = base64_encode('HgKQU2eVmxzf8UUQd2P25w9RubHXE2fH');
        $dataCoder = new \miniapp\AesEncryptUtil(1, $cms_aesKey);
        $result = $dataCoder->decrypt($code);
        $authData = authcode($result, 'DECODE');
        list($miniapp_id, $app_key, $type, $time) = explode('|', $authData);
        session('miniapp_id', $miniapp_id);
        // 查询是否存在该用户
        $userModel = new UserModel();
        $userDetail = $userModel->getDataDetail(['miniapp_id' => $miniapp_id]);
        if($userDetail){
            return json([
                'status' => 1,
                'msg' => '站点已初始化'
            ]);
        }else{
            // 启动事务
            Db::startTrans();
            try {
                // user表
                $userData = [
                    'miniapp_id' => $miniapp_id,
                    'miniapp_name' => $initData['miniapp_name'],
                    'nick_name' => '管理员',
                    'tel' => $initData['tel'],
                    'login' => random_string(),
                    'password' => md5('123456'),
                    'status' => 1,
                    'role' => $initData['type'] == 1?9:7,
                    'is_system' => 1,
                    'create_time' => time(),
                    'update_time' => time(),
                ];
                Db::name('user')->insert($userData);
                // form表
                $formData = [
                    'miniapp_id' => $miniapp_id,
                    'name' => '默认表单',
                    'config' => '[{"name":"\u624b\u673a\u53f7","length":"11","type":"phone","require":"1"},{"name":"\u59d3\u540d","length":"11","type":"text","require":"1"},{"name":"\u7559\u8a00","length":"","type":"textarea","require":"0"},{"name":"\u90ae\u7bb1","length":"","type":"email","require":"1"}]',
                    'status' => 1,
                    'is_default' => 1,
                    'create_time' => time(),
                    'update_time' => time(),
                ];
                Db::name('form')->insert($formData);
                // nav表
                $insertData = [];
                $defaultImg = 'https://qingzhan.ctrlyun.com/uploads/default';
                $navData = [
                    'name' => [
                        '首页',
                        '产品展示',
                        '一键导航',
                        '联系我们',
                        '留言',
                    ],
                    'path' => [
                        '/pages/index/index',
                        '/pages/tabBarTwo/tabBarTwo',
                        '/pages/tabBarThree/tabBarThree',
                        '/pages/tabBarFour/tabBarFour',
                        '/pages/tabBarFive/tabBarFive',
                    ],
                    'icon' => [
                        '/nav/index.png',
                        '/nav/product.png',
                        '/nav/map.png',
                        '/nav/about.png',
                        '/nav/message.png',
                    ],
                    'select_icon' => [
                        '/nav/index_active.png',
                        '/nav/product_active.png',
                        '/nav/map_active.png',
                        '/nav/about_active.png',
                        '/nav/message_active.png',
                    ],
                ];
                for ($i = 0; $i < 5; $i++) {
                    $insertData[$i] = [
                        'name' => $navData['name'][$i],
                        'path' => $navData['path'][$i],
                        'icon' => $defaultImg.$navData['icon'][$i],
                        'select_icon' => $defaultImg.$navData['select_icon'][$i],
                        'miniapp_id' => $miniapp_id,
                        'create_time' => time(),
                        'update_time' => time(),
                    ];
                }
                Db::name('nav')->insertAll($insertData);
                // setting表
                $settingData = [
                    [
                        'name' => 'site_info',
                        'miniapp_id' => $miniapp_id,
                    ],
                    [
                        'name' => 'site_style',
                        'miniapp_id' => $miniapp_id,
                    ]
                ];
                Db::name('setting')->insertAll($settingData);
                // seo表
                $seoData = [
                    'miniapp_id' => $miniapp_id,
                    'title' => '',
                    'keywords' => '',
                    'description' => '',
                    'sitemap' => '',
                    'url' => '',
                    'create_time' => time(),
                    'update_time' => time(),
                    'is_web' => 1,
                ];
                Db::name('seo')->insert($seoData);
                Db::commit();
            } catch (\Exception $e) {
                throw $e;
                // 回滚事务
                Db::rollback();
            }
            return json([
                'status' => 1,
                'msg' => '站点初始化成功'
            ]);
        }
    }


    // 更新数据
    public function updateData(Request $request)
    {
        $data = $request->post();
        Db::name('user')->where(['miniapp_id' => $data['miniapp_id']])->update(['miniapp_name' => $data['miniapp_name']]);
        return json([
            'status' => 1,
            'msg' => '数据更新成功'
        ]);
    }

}