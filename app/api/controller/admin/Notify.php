<?php

namespace app\api\controller\admin;

use think\Request;
use app\miniapp\model\Notify as NotifyModel;
use app\api\controller\cms\Base;

class Notify extends Base{

    public function index(Request $request)
    {
        $data = $request->post();
        $notifyModel = new NotifyModel();
        $notifyModel->insert($data);
        return json(['code' => 200, 'msg' => '成功']);
    }

    public function clear(Request $request)
    {
        $data = $request->post();
        $this->clearCache($data['name']);
    }

}