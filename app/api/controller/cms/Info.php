<?php
/*
 * @Description: 基本信息
 * @Author: WangHeng
 * @Date: 2019-08-11 16:17:10
 * @LastEditTime: 2019-08-19 16:19:21
 */

namespace app\api\controller\cms;

use app\admin\model\Setting as SettingModel;
use app\admin\model\Nav as NavModel;
use app\miniapp\model\Seo as SeoModel;

class Info extends Base{

    /**
     * 
     * @api {post} /api/cms/info 基本信息
     * @apiName 基本信息
     * @apiGroup Index
     * @apiVersion  1.0.0
     * 
     * 
     * @apiParam  {Integer} appid 小程序id
     * 
     * @apiSuccess (200) {Array} siteInfo 站点信息数组
     * @apiSuccess (200) {String} siteInfo:name 站点名称
     * @apiSuccess (200) {String} siteInfo:tel 电话号码
     * @apiSuccess (200) {String} siteInfo:bg 背景
     * @apiSuccess (200) {String} siteInfo:logo 站点logo
     * @apiSuccess (200) {String} siteInfo:address 站点地址
     * @apiSuccess (200) {String} siteInfo:lng 纬度
     * @apiSuccess (200) {String} siteInfo:lat 经度
     * @apiSuccess (200) {String} siteInfo:content 站点内容
     * @apiSuccess (200) {String} siteInfo:copyright 站点版权
     * @apiSuccess (200) {Array} siteStyle 站点样式数组
     * @apiSuccess (200) {String} siteStyle:nav_font 导航字体颜色
     * @apiSuccess (200) {String} siteStyle:nav_color 导航颜色
     * @apiSuccess (200) {String} siteStyle:tel_color 电话颜色
     * @apiSuccess (200) {String} siteStyle:tel_icon_color 电话图标颜色
     * @apiSuccess (200) {String} siteStyle:footer_color 底部导航颜色
     * @apiSuccess (200) {String} siteStyle:message_color 留言颜色
     * @apiSuccess (200) {String} siteStyle:category_color 分类导航颜色
     * @apiSuccess (200) {Array} navList 底部导航数组
     * @apiSuccess (200) {String} navList:name 底部导航名称
     * @apiSuccess (200) {String} navList:icon 图标
     * @apiSuccess (200) {String} navList:select_icon 选中图标
     * @apiSuccess (200) {String} navList:skip_id 跳转ID
     * @apiSuccess (200) {String} navList:path 跳转地址
     * 
     */
    public function index()
    {

        // 判断是否存在缓存
        $siteData = $this->getCache('siteData');
        if(!$siteData){
            // 获取基本信息
            $settingModel = new SettingModel();
            $siteInfo = $settingModel->getSettingValue('site_info');
            $siteStyle = $settingModel->getSettingValue('site_style');
            // 获取底部导航
            $navModel = new NavModel();
            $navList = $navModel->field('name,icon,select_icon,skip_id,path')->where(['miniapp_id' => $this->miniapp_id,'delete_time' => 0])->select()
            ->withAttr('path', function($value, $data) {
                return $value.'?name='.$data['name'].'&skip_id='.$data['skip_id'];
            })->toArray();
            // $navList = $navModel->getDataList([],'order desc','name,icon,select_icon,skip_id,path');
            // 获取seo
            $seoModel  = new SeoModel();
            $seoInfo = $seoModel->getDataDetail([],'title,keywords,description');

            // 设置缓存
            $siteData = [
                'siteInfo' => $siteInfo,
                'siteStyle' => $siteStyle,
                'navList' => $navList,
                'seoInfo' => $seoInfo,
            ];
            $this->setCache('siteData',$siteData);
        }
        $this->result($siteData,200,'获取成功','json');
    }

}