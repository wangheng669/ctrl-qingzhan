<?php
/*
 * @Description: 表单
 * @Author: WangHeng
 * @Date: 2019-08-11 16:17:10
 * @LastEditTime: 2019-08-16 09:59:16
 */

namespace app\api\controller\cms;

use app\cms\model\Form as FormModel;
use app\cms\model\Message as MessageModel;

class Form extends Base{

    /**
     * 
     * @api {post} /api/cms/form/read 表单详情
     * @apiName 表单详情
     * @apiGroup Form
     * @apiVersion  1.0.0
     * 
     * 
     * @apiParam  {Integer} appid 小程序id
     * @apiParam  {Integer} appid 表单id
     * 
     * @apiSuccess (200) {Array} FormDetail 表单数组
     * @apiSuccess (200) {String} FormDetail:name 表单名称
     * @apiSuccess (200) {Array} FormDetail:config 表单配置
     * @apiSuccess (200) {Array} FormDetail:config 表单配置
     * @apiSuccess (200) {String} FormDetail:config:name 配置名称
     * @apiSuccess (200) {String} FormDetail:config:length 配置长度
     * @apiSuccess (200) {String} FormDetail:config:field 配置类型
     * @apiSuccess (200) {String} FormDetail:config:require 是否必填
     * 
     */
    public function read($id=0)
    {
        // 默认表单
        if(!$id){
            $condition = ['is_default' => 1];
        }else{
            $condition = ['id' => $id];
        }
        // 获取表单详情
        $formModel = new FormModel();
        $formDetail = $formModel->getDataDetail($condition,'name,config');
        $this->result([
            'formDetail' => $formDetail,
        ],200,'获取成功','json');
    }

    /**
     * 
     * @api {post} /api/cms/form/message 留言
     * @apiName 表单留言
     * @apiGroup Form
     * @apiVersion  1.0.0
     * 
     * 
     * @apiParam  {String} appid 小程序id
     * 
     * @apiSuccess (200) {Array} data 提交返回
     * 
     */
    public function message()
    {
        $data = request()->post();
        $formModel = new FormModel();
        $messageModel = new MessageModel();
        if(!$data['form_id']){
            // 获取默认表单
            $formDetail = $formModel->getDataDetail(['is_default' => 1]);
        }else{
            // 获取表单详情
            $formDetail = $formModel->getDataDetail(['id' => $data['form_id']]);
        }
        if(!$formDetail){
            $this->result([],200,'表单不存在','json');
        }
        $data['miniapp'] = $this->miniapp_id;
        // 插入表
        $messageModel->allowField(true)->save($data);
        $this->result([],200,'提交成功','json');
    }

}