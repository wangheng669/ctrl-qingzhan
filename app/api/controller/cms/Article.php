<?php
/*
 * @Description: 文章
 * @Author: WangHeng
 * @Date: 2019-08-11 16:17:10
 * @LastEditTime: 2019-09-19 19:10:30
 */

namespace app\api\controller\cms;

use app\cms\model\Article as ArticleModel;
use app\cms\model\Category as CategoryModel;

class Article extends Base{


    /**
     * 
     * @api {post} /api/cms/article/read 文章详情
     * @apiName 详情
     * @apiGroup Article
     * @apiVersion  1.0.0
     * 
     * 
     * @apiParam  {Integer} appid 小程序id
     * @apiParam  {Integer} id 文章id
     * 
     * @apiSuccess (200) {Array} articleDetail 文章数组
     * @apiSuccess (200) {String} articleDetail:title 文章标题
     * @apiSuccess (200) {String} articleDetail:description 文章描述
     * @apiSuccess (200) {String} articleDetail:content 文章内容
     * @apiSuccess (200) {String} articleDetail:photos 文章相册
     * @apiSuccess (200) {String} articleDetail:seo_title seo标题
     * @apiSuccess (200) {String} articleDetail:seo_keywords seo关键字
     * @apiSuccess (200) {String} articleDetail:seo_description seo描述
     * @apiSuccess (200) {String} articleDetail:is_top 是否置顶
     * @apiSuccess (200) {String} articleDetail:is_recommend 是否推荐
     * @apiSuccess (200) {String} articleDetail:is_release 是否发布
     * @apiSuccess (200) {String} articleDetail:thumbnail 缩略图
     * @apiSuccess (200) {String} articleDetail:update_time 更新时间
     * @apiSuccess (200) {String} articleDetail:click_count 点击量
     * 
     */
    public function read($id=0,$type=0)
    {
        $articleModel = new ArticleModel();
        $categoryModel = new CategoryModel();
        $article_field = 'id,title,category_ids,description,desc,field,content,photos,seo_title,seo_keywords,seo_description,is_top,is_recommend,is_release,thumbnail,update_time,click_count,url';
        if($type == 1){
            // 单页
            $articleDetail = $articleModel->getDataDetail(['category_ids' => $id],$article_field);
        }else{
            // 获取文章详情
            $articleDetail = $articleModel->getDataDetail(['id' => $id],$article_field);
        }
        $category_id = explode(',',$articleDetail['category_ids']);
        $category = $categoryModel->getDataDetail(['id' => end($category_id)],'field,form_id,form_type');
        $articleDetail['form_id'] = $category['form_id'];
        $articleDetail['form_type'] = $category['form_type'];

        // 增加阅读数
        $articleModel->where(['id' => $id])->setInc('click_count');
        
        // 获取相关文章
        $otherArticleList = $articleModel->where([['category_ids','in',$articleDetail['category_ids']],['id','neq',$articleDetail['id']]])->where(['delete_time' => 0])->orderRaw('rand()')->limit(4)->field('id,title,desc,thumbnail')->select();
        $this->result([
            'articleDetail' => $articleDetail,
            'otherArticleList' => $otherArticleList
        ],200,'获取成功','json');
    }

    
    /**
     * 
     * @api {post} /api/cms/article/index 列表
     * @apiName 文章列表
     * @apiGroup Article
     * @apiVersion  1.0.0
     * 
     * 
     * @apiParam  {Integer} appid 小程序id
     * @apiParam  {Integer} cid 分类id
     * @apiParam  {Integer} index 位置
     * @apiParam  {Integer} size 数量
     * 
     * @apiSuccess (200) {Array} articleList 文章数组
     * @apiSuccess (200) {String} articleList:title 文章标题
     * @apiSuccess (200) {String} articleList:description 文章描述
     * @apiSuccess (200) {String} articleList:thumbnail 文章缩略图
     * @apiSuccess (200) {String} articleList:update_time 文章更新时间
     * @apiSuccess (200) {String} articleList:click_count 文章点击数
     * 
     */
    public function index($cid=0, $index=0, $size=4)
    {
        // 获取文章列表
        $articleModel = new ArticleModel();
        $articleList = $articleModel->getDataList("FIND_IN_SET($cid,category_ids)",'id desc','id,title,description,desc,thumbnail,update_time,click_count,url,field','',1,$index.','.$size);
        $this->result([
            'articleList' => $articleList,
        ],200,'获取成功','json');
    }



}