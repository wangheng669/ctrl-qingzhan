<?php
/*
 * @Description: 通知
 * @Author: WangHeng
 * @Date: 2019-08-11 16:17:10
 * @LastEditTime: 2019-08-14 16:03:59
 */

namespace app\api\controller\cms;

use app\cms\model\Notice as NoticeModel;

class Notice extends Base{

    /**
     * 
     * @api {post} /api/cms/Notice/read 通知详情
     * @apiName 通知列表
     * @apiGroup Notice
     * @apiVersion  1.0.0
     * 
     * 
     * @apiParam  {Integer} appid 小程序id
     * @apiParam  {Integer} id 通知id
     * 
     * @apiSuccess (200) {Array} NoticeDetail 通知数组
     * @apiSuccess (200) {String} NoticeDetail:id 通知ID
     * @apiSuccess (200) {String} NoticeDetail:title 通知标题
     * @apiSuccess (200) {String} NoticeDetail:content 通知内容
     * @apiSuccess (200) {String} NoticeDetail:seo_title seo标题
     * @apiSuccess (200) {String} NoticeDetail:seo_keywords seo关键字
     * @apiSuccess (200) {String} NoticeDetail:seo_description seo描述
     * @apiSuccess (200) {String} NoticeDetail:update_time 更新时间
     * 
     */
    public function read($id=0)
    {
        // 获取通知详情
        $NoticeModel = new NoticeModel();
        $NoticeDetail = $NoticeModel->getDataDetail(['id' => $id],'id,title,content,seo_title,seo_keywords,seo_description,update_time');
        $this->result([
            'Notice' => $NoticeDetail,
        ],200,'获取成功','json');
    }

}