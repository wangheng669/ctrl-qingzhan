<?php
/*
 * @Description: 分类
 * @Author: WangHeng
 * @Date: 2019-08-11 16:17:10
 * @LastEditTime: 2019-08-14 16:04:12
 */

namespace app\api\controller\cms;

use app\cms\model\Category as CategoryModel;

class Category extends Base{

    /**
     * 
     * @api {post} /api/cms/category/read 分类列表
     * @apiName 分类列表
     * @apiGroup Category
     * @apiVersion  1.0.0
     * 
     * 
     * @apiParam  {Integer} appid 小程序id
     * @apiParam  {Integer} id 分类id
     * 
     * @apiSuccess (200) {Array} categoryList 分类数组
     * @apiSuccess (200) {Array} categoryList:children 子分类数组
     * @apiSuccess (200) {String} categoryList:name 分类名称
     * @apiSuccess (200) {String} categoryList:path 分类路径
     * 
     */
    public function read($id=0)
    {
        // 获取分类列表
        $categoryModel = new CategoryModel();
        // 获取当前分类及子分类
        $categoryList = $categoryModel->getDataDetail(['id' => $id],'id,name,path,type,seo_title,seo_keywords,seo_description');
        $condition[] = ['path','like',$categoryList['path'].',%'];
        $children = $categoryModel->getDataList($condition,'id desc','id,name,type');
        $categoryList['children'] = $children;
        $this->result([
            'categoryList' => $categoryList,
        ],200,'获取成功','json');
    }

}