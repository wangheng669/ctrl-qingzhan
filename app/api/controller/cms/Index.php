<?php

namespace app\api\controller\cms;

use app\cms\model\Banner as BannerModel;
use app\cms\model\Adv as AdvModel;
use app\cms\model\Notice as NoticeModel;
use app\cms\model\Category as CategoryModel;
use app\cms\model\Article as ArticleModel;

class Index extends Base{


    /**
     * 
     * @api {post} /api/cms/index 首页
     * @apiName 首页
     * @apiGroup Index
     * @apiVersion  1.0.0
     * 
     * 
     * @apiParam  {Integer} appid 小程序id
     * 
     * @apiSuccess (200) {Array} bannerList 幻灯片数组
     * @apiSuccess (200) {String} bannerList:id 幻灯片ID
     * @apiSuccess (200) {String} bannerList:name 幻灯片名称
     * @apiSuccess (200) {String} bannerList:image 幻灯片图片
     * 
     * @apiSuccess (200) {Array} advList 广告数组
     * @apiSuccess (200) {String} advList:id 广告ID
     * @apiSuccess (200) {String} advList:name 广告名称
     * @apiSuccess (200) {String} advList:category_id 展现位置
     * @apiSuccess (200) {String} advList:image 广告图片
     * @apiSuccess (200) {String} advList:skip_id 广告跳转ID
     * @apiSuccess (200) {String} advList:skip_type 广告跳转类型
     *
     * @apiSuccess (200) {Array} noticeList 公告数组
     * @apiSuccess (200) {String} noticeList:id 公告ID
     * @apiSuccess (200) {String} noticeList:title 公告标题
     * @apiSuccess (200) {String} noticeList:update_time 更新时间
     *
     * @apiSuccess (200) {Array} navList 导航数组
     * @apiSuccess (200) {String} navList:id 导航ID
     * @apiSuccess (200) {String} navList:type 导航类型
     * @apiSuccess (200) {String} navList:name 导航名称
     * @apiSuccess (200) {String} navList:icon 导航图标
     *
     * @apiSuccess (200) {Array} categoryList 分类数组
     * @apiSuccess (200) {String} categoryList:id 分类ID
     * @apiSuccess (200) {String} categoryList:type 分类类型
     * @apiSuccess (200) {String} categoryList:name 分类名称
     * @apiSuccess (200) {Array} categoryList:article 分类下的文章
     * @apiSuccess (200) {Array} categoryList:article:id 文章ID
     * @apiSuccess (200) {Array} categoryList:article:description 文章描述
     * @apiSuccess (200) {Array} categoryList:article:thumbnail 文章缩略图
     * @apiSuccess (200) {Array} categoryList:article:update_time 文章更新时间
     * @apiSuccess (200) {Array} categoryList:article:click_count 文章点击量
     * 
     * 
     */
    public function index()
    {
        // 获取banner
        $bannerModel = new BannerModel();
        $bannerList = $bannerModel->getDataList([],'order desc','id,name,image');
        // 获取广告
        $advModel = new AdvModel();
        $advList = $advModel->getDataList([],'order desc','id,name,category_id,image,skip_id,skip_type,size');
        // 获取公告
        $noticeModel = new NoticeModel();
        $noticeList = $noticeModel->getDataList([],'order desc','id,title,update_time');
        // 获取导航
        $categoryModel = new CategoryModel();
        $navList = $categoryModel->getDataList(['nav_show' => 1],'nav_order desc','id,type,name,icon');
        // 获取分类以及文章
        $articleModel = new ArticleModel();
        $categoryList = $categoryModel->getDataList(['index_show' => 1],'index_order desc','id,type,name,show_count,path');
        foreach($categoryList as $category){
            $id = $category['id'];
            $category['article'] = $articleModel->getDataList("FIND_IN_SET($id,category_ids)",'id desc','id,title,description,desc,field,thumbnail,update_time,click_count,url','',1,$category['show_count']);
        }
        $this->result([
            'bannerList' => $bannerList,
            'advList' => $advList,
            'noticeList' => $noticeList,
            'navList' => $navList,
            'categoryList' => $categoryList,
        ],200,'获取成功','json');
    }

}