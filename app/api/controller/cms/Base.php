<?php

namespace app\api\controller\cms;

use think\Controller;
use think\facade\Cache;

class Base extends Controller
{

    // 小程序id
    protected $miniapp_id;

    protected function initialize()
    {
        // 获取appid
        $this->miniapp_id = request()->param('appid');
        session('miniapp_id',$this->miniapp_id);
    }

    // 获取redis缓存
    public function getCache($name)
    {
        $data = Cache::store('redis')->get($name.$this->miniapp_id);
        return $data;
    }
    
    // 设置redis缓存
    public function setCache($name,$data)
    {
        Cache::store('redis')->set($name.$this->miniapp_id,$data);
    }

    // 清除缓存
    public function clearCache($name)
    {
        Cache::store('redis')->set($name.$this->miniapp_id,null);
    }

}