<?php

namespace app\cms\model;

use app\common\model\BaseModel;

class Article extends BaseModel
{
    // 获取相册数据
    public function getPhotosAttr($value)
    {
        return json_decode($value,true);
    }

    // 设置描述 - 去除富文本
    public function setDescAttr($value,$data)
    {
        $description = strip_tags(str_replace("&nbsp;","",htmlspecialchars_decode($data['description'])));
        $desc = mb_substr($description, 0, 100,"utf-8");
        return $desc;
    }
    
    // 设置自定义字段
    public function setFieldAttr($value)
    {
        return json_encode($value);
    }
    
    // 返回自定义字段
    public function getFieldAttr($value)
    {
        return json_decode($value,1);
    }
    
    // 获取描述
    public function getDescriptionAttr($value)
    {
        return str_replace('p>','div>',$value);
    }

    // 判断是否有文章
    public function hasAtricle($id)
    {
        $result = $this->getDataDetail("FIND_IN_SET($id,category_ids)");
        if($result){
            return ['msg' => '存在文章!无法删除'];
        }
        return false;
    }

}
