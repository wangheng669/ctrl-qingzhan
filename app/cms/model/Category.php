<?php

namespace app\cms\model;

use app\common\model\BaseModel;

class Category extends BaseModel
{
    // 判断是否有子分类
    public function hasCategory($id)
    {
        $result = $this->getDataDetail([['pid','in',$id]]);
        if($result){
            return ['msg' => '存在子分类!无法删除'];
        }
        return false;
    }

    // 自定义字段转换
    public function getFieldAttr($value)
    {
        return json_decode($value,1);
    }

    // 分类树
    public function cateTree($condition=[]){
        $data=$this->getDataList($condition);//查询cate表中的数据
        $res = $this->cateSort($data);
        return $res;
    }
    
    //定义一个数组，每循环一条记录就把它放入该数组并unset该记录
    public function cateSort($data,$pid=0,$count=0){
       static $arr=array();//静态初始化
        foreach ($data as $k => $v){
            if($v['pid']==$pid){
                $v['count']=$count;
                $arr[]=$v;
                unset($data[$k]);//去掉不再使用的
                $this->cateSort($data,$v['id'],$count+1);
            }
        }
        return $arr;
    }

}
