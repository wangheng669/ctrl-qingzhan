<?php

namespace app\cms\model;

use app\common\model\BaseModel;

class Message extends BaseModel
{
    // 内容转换
    public function getFormContentAttr($value)
    {
        return json_decode($value,1);
    }
}
