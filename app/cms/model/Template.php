<?php

namespace app\cms\model;

use app\common\model\BaseModel;

class Template extends BaseModel
{
    // 获取模板配置
    public function getTemplateConfig($condition,$is_system)
    {
        if($is_system == 0){
            return $this->where($condition)->where(['template_id' => session('template_id'),'is_system' => $is_system])->column('img,name','type');
        }
        return $this->where($condition)->where(['template_id' => session('template_id'),'is_system' => $is_system])->column('img','type');
    }

}
