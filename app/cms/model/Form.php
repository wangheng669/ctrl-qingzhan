<?php

namespace app\cms\model;

use app\common\model\BaseModel;

class Form extends BaseModel
{
    // 获取表单配置
    public function getConfigAttr($value)
    {
        return json_decode($value,1);
    }
}
