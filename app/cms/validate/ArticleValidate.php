<?php
/*
 * @Description: 文章验证
 * @Author: WangHeng
 * @Date: 2019-07-22 22:16:32
 */
namespace app\cms\validate;

use app\common\validate\BaseValidate;

class ArticleValidate extends BaseValidate
{
    
	protected $rule = [
        'category_ids' => 'require',
        'title' => 'require',
        'description' => 'require',
        'content' => 'require',
        'url' => 'url',
    ];
    
    
    protected $message = [
        'category_ids.require' => '请填写文章分类',
        'title.require' => '请填写文章标题',
        'description.require' => '请填写文章描述',
        'content.require' => '请填写文章内容',
        'url.url' => '链接错误',
    ];
}
