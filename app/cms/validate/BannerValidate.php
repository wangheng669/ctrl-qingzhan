<?php
/*
 * @Description: 幻灯片验证
 * @Author: WangHeng
 * @Date: 2019-08-01 21:15:41
 * @LastEditTime: 2019-08-04 17:21:07
 */
namespace app\cms\validate;

use app\common\validate\BaseValidate;

class BannerValidate extends BaseValidate
{
    
	protected $rule = [
        'name' => 'require',
        'image' => 'require',
        'status' => 'require',
    ];
    
    
    protected $message = [
        'name.require' => '请填写幻灯片名称',
        'image.require' => '请选择幻灯片图片',
        'status.require' => '请选择幻灯片状态',
    ];
}
