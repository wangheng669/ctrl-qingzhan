<?php
/*
 * @Description: 表单验证
 * @Author: WangHeng
 * @LastEditTime: 2019-08-04 23:25:05
 */
namespace app\cms\validate;

use app\common\validate\BaseValidate;

class FormValidate extends BaseValidate
{
    
	protected $rule = [
        'name' => 'require',
        'config' => 'require',
        'status' => 'require',
    ];
    
    
    protected $message = [
        'name.require' => '请填写表单名称',
        'config.require' => '请填写配置项',
        'status.require' => '请填写状态',
    ];
}
