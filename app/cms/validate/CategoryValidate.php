<?php
/*
 * @Description: 分类验证
 * @Author: WangHeng
 * @Date: 2019-07-18 21:19:50
 */

namespace app\cms\validate;

use app\common\validate\BaseValidate;

class CategoryValidate extends BaseValidate
{
    
	protected $rule = [
        'pid' => 'require',
        'type' => 'require',
        'name' => 'require',
        'show_count' => 'integer',
        'index_show' => 'require|integer',
        'form_id' => 'require|integer',
        'form_type' => 'integer',
    ];
    
    
    protected $message = [
        'pid.require' => '请填写分类所属',
        'type.require' => '请选择分类类型',
        'name.require' => '请输入分类名称',
        'show_count.integer' => '文章数填写错误',
        'index_show.require' => '请选择首页状态',
        'index_show.integer' => '状态填写错误',
        'form_id.require' => '请选择留言表单',
        'form_id.integer' => '留言填写错误',
        'form_type.integer' => '表单类型错误',
    ];
}
