<?php
/*
 * @Description: 公告验证
 * @Author: WangHeng
 * @Date: 2019-07-15 22:12:07
 */

namespace app\cms\validate;

use app\common\validate\BaseValidate;

class NoticeValidate extends BaseValidate
{
    
	protected $rule = [
        'title' => 'require',
        'status' => 'require',
    ];
    
    
    protected $message = [
        'title.require' => '请填写标题',
        'status.require' => '请选择状态',
    ];
}
