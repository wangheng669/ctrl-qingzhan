<?php
/*
 * @Description: 广告验证
 * @Author: WangHeng
 * @LastEditTime: 2019-08-04 17:56:43
 */
namespace app\cms\validate;

use app\common\validate\BaseValidate;

class AdvValidate extends BaseValidate
{
    
	protected $rule = [
        'name' => 'require',
        'category_id' => 'require',
        'size' => 'require',
        'image' => 'require',
        'skip_type' => 'require',
        'skip_id' => 'require',
        'status' => 'require',
    ];
    
    
    protected $message = [
        'name.require' => '请填写广告名称',
        'category_id.require' => '请选择广告位置',
        'size.require' => '请选择广告尺寸',
        'image.require' => '请填写广告图片',
        'skip_type.require' => '请选择广告类型',
        'skip_id.require' => '请选择跳转位置',
        'status.require' => '请选择广告状态',
    ];
}
