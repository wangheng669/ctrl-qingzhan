<?php

use think\Db;

function category_line($value)
{
    return str_repeat('&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',$value);
}

// 表单默认
function is_default($value,$id)
{
    if($value == 1){
        return '<span class="layui-badge btn-operate layui-bg-green" data-href="setDefaultForm?id='.$id.'">默认</span>';
    }else{
        return '<span class="layui-badge btn-operate layui-bg-gray" data-href="setDefaultForm?id='.$id.'">非默认</span>';
    }
}

// 文章显示分类名称
function article_category($value)
{
    $result = Db::name('category')->where([['id','in',$value]])->column('name');
    $result = implode(',',$result);
    return $result;
}

// 广告分类转换
function adv_category($value)
{
    $result = Db::name('category')->where(['id' => $value])->value('name');
    return $result;
}