<?php
/*
 * @Description: 幻灯片管理
 * @Author: WangHeng
 * @Date: 2019-08-01 20:41:20
 * @LastEditTime: 2019-08-27 13:40:00
 */

namespace app\cms\controller;

use think\Request;
use app\common\controller\Base;
use app\cms\model\Banner as BannerModel;
use app\cms\validate\BannerValidate;

class Banner extends Base
{
    
    /**
     * 初始操作
     * @author WangHeng
     */
    protected function initialize()
    {
        parent::initialize();
        // 获取模板配置
        $condition[] = ['type','in','banner'];
        $this->getTemplateConfig($condition,1);
    }

    public function index(Request $request)
    {
        $data = $request->param();
        $condition = [];
        $start = empty($data['start'])?'':$data['start'];
        $name = empty($data['name'])?'':$data['name'];
        $end = empty($data['end'])?'':$data['end'];
        if($start){
            $condition[] = ['create_time','egt' ,strtotime($start)];
        }
        if($end){
            $condition[] = ['create_time','lt' ,strtotime($end)];
        }
        if($name){
            $condition[]= ['name','like', "%{$name}%"];
        }
        $order = 'id desc';
        $bannerModel = new BannerModel();
        $bannerList = $bannerModel->getDataList($condition,$order,'*',10);
        $this->assign('bannerList',$bannerList);
        $this->assign('name',$name);
        $this->assign('start',$start);
        $this->assign('end',$end);
        return $this->fetch();
    }

    public function add(Request $request)
    {
        if($request->isPost()){
            (new BannerValidate())->goCheck($request);
            $data = $request->post();
            $bannerModel = new BannerModel();
            $data['miniapp_id'] = $this->miniapp_id;
            $bannerModel->allowField(true)->save($data);
            $this->log('添加幻灯片 名称:'.$data['name']);
            $this->success('添加成功','',['status' => 1]);
        }else{
            return $this->fetch();
        }
    }
    
    public function edit(Request $request)
    {
        $id = $request->param('id');
        $bannerModel = new BannerModel();
        if($request->isPost()){
            (new BannerValidate())->goCheck($request);
            $data = $request->post();
            $bannerModel->allowField(true)->update($data,['id' => $id]);
            $this->log('编辑幻灯片 名称:'.$data['name']);
            $this->success('保存成功','',['status' => 1]);
        }else{
            $condition = [
                'id' => $id,
            ];
            $bannerDetail = $bannerModel->getDataDetail($condition);
            $this->assign('bannerDetail',$bannerDetail);
            return $this->fetch();
        }
    }

    // 更新字段值
    public function field(Request $request)
    {
        $data = $request->param();
        $bannerModel = new BannerModel();
        $result = $bannerModel->updateField($data);
        $this->success($result['msg'],'',['status' => $result['status']]);
    }

    // 删除
    public function delete(Request $request)
    {
        $id = $request->param('id');
        $bannerModel = new BannerModel();
        $result = $bannerModel->deleteData($id);
        $this->success($result);
    }

}
