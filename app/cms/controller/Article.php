<?php
/*
 * @Description: 文章
 * @Author: WangHeng
 * @Date: 2019-07-11 23:09:17
 * @LastEditTime: 2019-08-19 13:47:04
 */

namespace app\cms\controller;

use think\Request;
use app\common\controller\Base;
use app\cms\model\Category as CategoryModel;
use app\cms\model\Article as ArticleModel;
use app\cms\model\Template as TemplateModel;
use app\cms\validate\ArticleValidate;

class Article extends Base
{
    
    /**
     * 初始操作
     * @author WangHeng
     */
    protected function initialize()
    {
        parent::initialize();
        // 获取模板配置
        $condition[] = ['type','in','p_slide'];
        $this->getTemplateConfig($condition,1);
        // 获取分类模板配置
        $templateModel = new TemplateModel();
        $categoryConfig = $templateModel->getTemplateConfig([],0);
        $this->assign('categoryConfig',json_encode($categoryConfig));
    }

    public function index(Request $request)
    {
        $data = $request->param();
        $condition[] = ['delete_time','eq',0];
        $start = empty($data['start'])?'':$data['start'];
        $title = empty($data['title'])?'':$data['title'];
        $end = empty($data['end'])?'':$data['end'];
        if($start){
            $condition[] = ['create_time','egt' ,strtotime($start)];
        }
        if($end){
            $condition[] = ['create_time','lt' ,strtotime($end)];
        }
        if($title){
            $condition[]= ['title','like', "%{$title}%"];
        }
        $order = 'id desc';
        // 获取文章列表
        $articleModel = new ArticleModel();
        $articleList = $articleModel->getDataList($condition,$order,'*',10);
        $this->assign('articleList',$articleList);
        $this->assign('title',$title);
        $this->assign('start',$start);
        $this->assign('end',$end);
        return $this->fetch();
    }

    // 添加
    public function add(Request $request)
    {
        $cid = $request->param('cid');
        if($request->isPost()){
            (new ArticleValidate())->goCheck($request);
            $data = $request->post();
            $articleModel = new ArticleModel();
            $data['miniapp_id'] = $this->miniapp_id;
            $data['photos'] = empty($data['photo'])?'':json_encode($data['photo']);
            $articleModel->allowField(true)->save($data);
            $this->log('添加文章 名称:'.$data['title']);
            $this->success('添加成功','',['status' => 1]);
        }else{
            $categoryModel = new CategoryModel();
            $categoryList = $categoryModel->getDataList([],'path asc');
            $categoryDetail = $categoryModel->getDataDetail(['id' => $cid],'field');
            $this->assign('categoryList',$categoryList);
            $this->assign('categoryDetail',$categoryDetail);
            $this->assign('cid',$cid);
            return $this->fetch();
        }
    }
    
    // 编辑
    public function edit(Request $request)
    {
        $id = $request->param('id');
        $articleModel = new ArticleModel();
        if($request->isPost()){
            (new ArticleValidate())->goCheck($request);
            $data = $request->post();
            $data['photos'] = empty($data['photo'])?'':json_encode($data['photo']);
            $articleModel->allowField(true)->update($data,['id' => $id]);
            $this->log('编辑文章 名称:'.$data['title']);
            $this->success('保存成功','',['status' => 1]);
        }else{
            $categoryModel = new CategoryModel();
            $categoryList = $categoryModel->getDataList([],'path asc');
            $articleDetail = $articleModel->getDataDetail(['id' => $id],'*');
            $category_ids = explode(',',$articleDetail['category_ids']);
            $articleDetail['category_ids'] = end($category_ids);
            $categoryDetail = $categoryModel->getDataDetail(['id' => $articleDetail['category_ids']],'field');
            $this->assign('categoryList',$categoryList);
            $this->assign('articleDetail',$articleDetail);
            $this->assign('categoryDetail',$categoryDetail);
            return $this->fetch();
        }
    }

    // 删除
    public function delete(Request $request)
    {
        $id = $request->param('id');
        $articleModel = new ArticleModel();
        $result = $articleModel->deleteData($id);
        $this->success($result);
    }

    // 更新字段值
    public function field(Request $request)
    {
        $data = $request->param();
        $articleModel = new ArticleModel();
        $result = $articleModel->updateField($data);
        $this->success($result['msg'],'',['status' => $result['status']]);
    }

    // 编辑单页
    public function page(Request $request)
    {
        $id = $request->param('id');
        $articleModel = new ArticleModel();
        $condition = ['category_ids' => $id];
        $articleDetail = $articleModel->getDataDetail($condition,'*');
        $this->assign('articleDetail',$articleDetail);
        return $this->fetch();
    }

    // 分类下的文章
    public function list(Request $request)
    {
        $data = $request->param();
        $cid = $data['cid'];
        $order = 'id desc';
        // 获取文章列表
        $articleModel = new ArticleModel();
        $articleList = $articleModel->getDataList("FIND_IN_SET($cid,category_ids)",$order,'*',10);
        $this->assign('articleList',$articleList);
        $this->assign('cid',$cid);
        return $this->fetch();
    }

}
