<?php

namespace app\cms\controller;

use think\Request;
use app\common\controller\Base;
use app\cms\model\Form as FormModel;
use app\cms\validate\FormValidate;
use app\cms\model\Message as MessageModel;

class Form extends Base
{
    
    public function index()
    {
        $formModel = new FormModel();
        $formList = $formModel->getDataList();
        $this->assign('formList',$formList);
        return $this->fetch();
    }

    // 添加
    public function add(Request $request)
    {
        if($request->isPost()){
            (new FormValidate())->goCheck($request);
            $data = $request->post();
            $data['miniapp_id'] = $this->miniapp_id;
            $config = [];
            foreach($data['config'] as $name=>$configs){
                foreach($configs as $k=>$v){
                    $config[$k][$name] = $v;
                }
            }
            $data['config'] = json_encode($config);
            $formModel = new FormModel();
            $formModel->allowField(true)->save($data);
            $this->log('添加表单 名称:'.$data['name']);
            $this->success('添加成功','',['status' => 1]);
        }else{
            return $this->fetch();
        }
    }
    
    // 编辑
    public function edit(Request $request)
    {
        $id = $request->param('id');
        $formModel = new FormModel();
        if($request->isPost()){
            (new FormValidate())->goCheck($request);
            $data = $request->post();
            $data['miniapp_id'] = $this->miniapp_id;
            $config = [];
            foreach($data['config'] as $name=>$configs){
                foreach($configs as $k=>$v){
                    $config[$k][$name] = $v;
                }
            }
            $data['config'] = json_encode($config);
            $formModel->allowField(true)->update($data,['id' => $id,'miniapp_id' => $this->miniapp_id]);
            $this->log('保存表单 名称:'.$data['name']);
            $this->success('保存成功','',['status' => 1]);
        }else{
            $formDetail = $formModel->getDataDetail(['id' => $id]);
            $this->assign('formDetail',$formDetail);
            return $this->fetch();
        }
    }

    // 删除
    public function delete(Request $request)
    {
        $id = $request->param('id');
        $formModel = new FormModel();
        $result = $formModel->deleteData(['id' => $id]);
        $this->success($result);
    }

    // 更新字段值
    public function field(Request $request)
    {
        $data = $request->param();
        $formModel = new FormModel();
        $result = $formModel->updateField($data);
        $this->success($result['msg'],'',['status' => $result['status']]);
    }

    // 更新默认表单
    public function setDefaultForm(Request $request)
    {
        $id = $request->param('id');
        $formModel = new FormModel();
        $formModel->update(['is_default' => 0],['miniapp_id' => session('miniapp_id'),'is_default' => 1]);
        $formModel->update(['is_default' => 1],['miniapp_id' => session('miniapp_id'),'id' => $id]);
        $this->success('更新成功','',['status' => 1]);
    }

    public function message(Request $request)
    {
        $form_id = $request->param('id');
        $messageModel = new MessageModel();
        $messageList = $messageModel->getDataList(['form_id' => $form_id],'','*','',0);
        $this->assign('messageList',$messageList);
        return $this->fetch();
    }


}
