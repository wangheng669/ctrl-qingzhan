<?php

namespace app\cms\controller;

use think\Request;
use app\common\controller\Base;
use app\cms\model\Category as CategoryModel;
use app\cms\model\Article as ArticleModel;
use app\cms\validate\CategoryValidate;
use app\cms\service\CategoryService;
use app\cms\model\Form as FormModel;
use app\cms\model\Template as TemplateModel;

class Category extends Base
{

    /**
     * 初始操作
     * @author WangHeng
     */
    protected function initialize()
    {
        parent::initialize();
        // 获取模板配置
        $condition[] = ['type','in','nav'];
        $this->getTemplateConfig([],0);
        $template = new TemplateModel();
        $templateConfig = $template->getTemplateConfig($condition,1);
        $this->assign('templateConfig',$templateConfig);
    }

    public function index(Request $request)
    {        
        $data = $request->param();
        $condition = [];
        $start = empty($data['start'])?'':$data['start'];
        $name = empty($data['name'])?'':$data['name'];
        $end = empty($data['end'])?'':$data['end'];
        if($start){
            $condition[] = ['create_time','egt' ,strtotime($start)];
        }
        if($end){
            $condition[] = ['create_time','lt' ,strtotime($end)];
        }
        if($name){
            $condition[]= ['name','like', "%{$name}%"];
        }
        $categoryModel = new CategoryModel();
        $categoryList = $categoryModel->getDataList($condition,'path asc');
        $this->assign('categoryList',$categoryList);
        $this->assign('name',$name);
        $this->assign('start',$start);
        $this->assign('end',$end);
        return $this->fetch();
    }

    // 添加
    public function add(Request $request)
    {
        $id = $request->param('id',0);
        if($request->isPost()){
            (new CategoryValidate())->goCheck($request);
            $data = $request->post();
            $field = [];
            if(empty($data['is_field'])){
                $data['field'] = '';
            }else{
                foreach($data['field'] as $name=>$fields){
                    foreach($fields as $k=>$v){
                        $field[$k][$name] = $v;
                    }
                }
                $data['field'] = json_encode($field);
            }
            $data['miniapp_id'] = $this->miniapp_id;
            $categoryService = new CategoryService();
            $categoryService->addCategory($data);
            $this->log('添加分类 名称:'.$data['name']);
            $this->success('添加成功','',['status' => 1]);
        }else{
            // 获取分类列表
            $categoryModel = new CategoryModel();
            $formModel = new FormModel();
            $formList = $formModel->getDataList();
            $categoryList = $categoryModel->getDataList([],'path asc');
            $this->assign('categoryList',$categoryList);
            $this->assign('formList',$formList);
            $this->assign('id',$id);
            return $this->fetch();
        }
    }
    
    // 编辑
    public function edit(Request $request)
    {
        // 获取分类数据
        if($request->isPost()){
            (new CategoryValidate())->goCheck($request);
            $data = $request->post();
            $field = [];
            if(empty($data['is_field'])){
                $data['field'] = '';
            }else{
                foreach($data['field'] as $name=>$fields){
                    foreach($fields as $k=>$v){
                        $field[$k][$name] = $v;
                    }
                }
                $data['field'] = json_encode($field);
            }
            $categoryService = new CategoryService();
            $result = $categoryService->editCategory($data);
            if(!$result['status']){
                $this->success($result['msg'],'',['status' => 2,'is_load' => 2]);
            }
            $this->log('编辑分类 名称:'.$data['name']);
            $this->success('保存成功','',['status' => 1]);
        }else{
            $id = $request->param('id');
            $categoryModel = new CategoryModel();
            $categoryDetail = $categoryModel->getDataDetail(['id' => $id],'*');
            // 获取分类列表
            $condition[] = ['id','neq',$id];
            $categoryList = $categoryModel->getDataList($condition,'path asc');
            $formModel = new FormModel();
            $formList = $formModel->getDataList();
            $this->assign('categoryList',$categoryList);
            $this->assign('categoryDetail',$categoryDetail);
            $this->assign('formList',$formList);
            return $this->fetch();
        }
    }

    // 删除
    public function delete(Request $request)
    {
        $id = $request->param('id');
        // 判断是否有子分类
        $categoryModel = new CategoryModel();
        $articleModel = new ArticleModel();
        $result = $categoryModel->hasCategory($id);
        if($result){
            $this->success($result['msg'],'',['status' => 0]);
        }
        
        $categoryDetail = $categoryModel->getDataDetail(['id' => $id]);
        if($categoryDetail['type'] == 3){
            // 判断是否为单页 删除分类下的文章
            $articleModel->deleteData($id);
        }else{
            // 判断是否有文章
            $ids = explode(',',$id);
            foreach($ids as $articleId){
                $result = $articleModel->hasAtricle($articleId);
                if($result){
                    $this->success($result['msg'],'',['status' => 0]);
                }
            }
        }
        $result = $categoryModel->deleteData($id);
        $this->success($result,'',['status' => 1]);
    }

    // 更新字段值
    public function field(Request $request)
    {
        $data = $request->param();
        $categoryModel = new CategoryModel();
        $result = $categoryModel->updateField($data);
        $this->success($result['msg'],'',['status' => $result['status']]);
    }

}
