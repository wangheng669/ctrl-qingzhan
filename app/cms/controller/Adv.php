<?php
/*
 * @Description: 广告管理
 * @Author: WangHeng
 * @Date: 2019-07-11 23:09:32
 * @LastEditTime: 2019-08-28 20:42:35
 */

namespace app\cms\controller;

use think\Request;
use app\common\controller\Base;
use app\cms\model\Category as CategoryModel;
use app\cms\model\Article as ArticleModel;
use app\cms\model\Adv as AdvModel;
use app\cms\validate\AdvValidate;

class Adv extends Base
{

    /**
     * 初始操作
     * @author WangHeng
     */
    protected function initialize()
    {
        parent::initialize();
        // 获取模板配置
        $condition[] = ['type','in','adv'];
        $this->getTemplateConfig($condition,1);
    }
    
    public function index()
    {
        $advModel = new AdvModel();
        $advList = $advModel->getDataList();
        $this->assign('advList',$advList);
        return $this->fetch();
    }

    // 添加
    public function add(Request $request)
    {
        if($request->isPost()){
            (new AdvValidate())->goCheck($request);
            $data = $request->post();
            $advModel = new AdvModel();
            $data['miniapp_id'] = $this->miniapp_id;
            $advModel->allowField(true)->save($data);
            $this->log('添加广告 名称:'.$data['name']);
            $this->success('添加成功','',['status' => 1]);
        }else{
            $categoryModel = new CategoryModel();
            $categoryList = $categoryModel->getDataList();
            $this->assign('categoryList',$categoryList);
            return $this->fetch();
        }
    }
    
    // 编辑
    public function edit(Request $request)
    {
        $id = $request->param('id');
        $advModel = new AdvModel();
        if($request->isPost()){
            (new AdvValidate())->goCheck($request);
            $data = $request->post();
            $advModel->allowField(true)->update($data,['id' => $id,'miniapp_id' => $this->miniapp_id]);
            $this->log('编辑广告 名称:'.$data['name']);
            $this->success('保存成功','',['status' => 1]);
        }else{
            $categoryModel = new CategoryModel();
            $articleModel = new ArticleModel();
            $advDetail = $advModel->getDataDetail(['id' => $id]);
            $categoryList = $categoryModel->getDataList();
            if($advDetail['skip_type'] == 2){
                $skipList = $articleModel->getDataList([],'id desc','id,title as name');
            }else{
                $skipList = $categoryList;
            }
            $this->assign('categoryList',$categoryList);
            $this->assign('skipList',$skipList);
            $this->assign('advDetail',$advDetail);
            return $this->fetch();
        }
    }

    // 获取跳转列表
    public function skipList(Request $request)
    {
        $type = $request->param('type');
        // 分类列表
        if($type == 1){
            $categoryModel = new CategoryModel();
            $skipList = $categoryModel->getDataList([],'id desc','id,name');
        }else{
            $articleModel = new ArticleModel();
            $skipList = $articleModel->getDataList([],'id desc','id,title name');
        }
        return $skipList;
    }

    // 更新字段值
    public function field(Request $request)
    {
        $data = $request->param();
        $advModel = new AdvModel();
        $result = $advModel->updateField($data);
        $this->success($result['msg'],'',['status' => $result['status']]);
    }

    // 删除
    public function delete(Request $request)
    {
        $id = $request->param('id');
        $advModel = new AdvModel();
        $result = $advModel->deleteData($id);
        $this->success($result);
    }

}
