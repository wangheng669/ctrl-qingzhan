<?php

namespace app\cms\controller;

use think\Request;
use app\common\controller\Base;
use app\cms\validate\NoticeValidate;
use app\cms\model\Notice as NoticeModel;

class Notice extends Base
{
    public function index(Request $request)
    {
        $noticeModel = new NoticeModel();
        $data = $request->param();
        $condition = [];
        $start = empty($data['start'])?'':$data['start'];
        $title = empty($data['title'])?'':$data['title'];
        $end = empty($data['end'])?'':$data['end'];
        if($start){
            $condition['create_time'] = ['egt' ,strtotime($start)];
        }
        if($end){
            $condition['create_time'] = ['lt' ,strtotime($end)];
        }
        if($title){
            $condition = [
                ['title', 'like', "%".$title."%"],
            ];
        }
        $noticeList = $noticeModel->getDataList($condition,'id desc','*',10);
        $this->assign('noticeList',$noticeList);
        $this->assign('title',$title);
        $this->assign('start',$start);
        $this->assign('end',$end);
        return $this->fetch();
    }

    // 添加
    public function add(Request $request)
    {
        if($request->isPost()){
            (new NoticeValidate())->goCheck($request);
            $data = $request->post();
            $noticeModel = new NoticeModel();
            $data['miniapp_id'] = $this->miniapp_id;
            $noticeModel->allowField(true)->save($data);
            $this->log('添加公告 名称:'.$data['title']);
            $this->success('添加成功','',['status' => 1]);
        }
        return $this->fetch();
    }
    
    // 编辑
    public function edit(Request $request)
    {
        $noticeModel = new NoticeModel();
        $id = $request->param('id');
        if($request->isPost()){
            (new NoticeValidate())->goCheck($request);
            $data = $request->post();
            $noticeModel->allowField(true)->update($data,['id' => $id,'miniapp_id' => $this->miniapp_id]);
            $this->log('编辑公告 名称:'.$data['title']);
            $this->success('保存成功','',['status' => 1]);
        }
        $noticeDetail = $noticeModel->getDataDetail(['id' => $id]);
        $this->assign('noticeDetail',$noticeDetail);
        return $this->fetch();
    }

    // 删除
    public function delete(Request $request)
    {
        $id = $request->param('id');
        $noticeModel = new NoticeModel();
        $result = $noticeModel->deleteData(['id' => $id]);
        $this->success($result);
    }

    // 更新字段值
    public function field(Request $request)
    {
        $data = $request->param();
        $noticeModel = new NoticeModel();
        $result = $noticeModel->updateField($data);
        $this->success($result['msg'],'',['status' => $result['status']]);
    }


}
