<?php
/*
 * @Description: 分类服务层
 * @Author: WangHeng
 * @Date: 2019-07-24 23:28:05
 */

namespace app\cms\service;

use app\cms\model\Category as CategoryModel;
use app\cms\model\Article as ArticleModel;

class CategoryService
{

    // 添加分类
    public function addCategory($data)
    {
        $categoryModel = new CategoryModel();
        $category = $categoryModel->allowField(true)->create($data);

        // 判断是否有父级分类
        if (empty($data['pid'])){
            $category->path = '0,'.$category->id;
        } else {
            $parentPath = $categoryModel->where('id',$data['pid'])->value('path');
            $category->path = $parentPath.','.$category->id;
            $category->level = count(explode(',',$category->path)) -1;
        }
        $category->save();
        // 判断分类类型 增加单页
        if($data['type'] == 3){
            $articleModel = new ArticleModel();
            $articleData = [
                'title' => $data['name'],
                'category_ids' => $category->id,
                'miniapp_id' => $data['miniapp_id'],
            ];
            $articleModel->save($articleData);
        }
    }

    // 编辑分类
    public function editCategory($data)
    {
        $id = $data['id'];
        $pid = $data['pid'];
        $categoryModel = new CategoryModel();
        $cateogryDetail = $categoryModel->getDataDetail(['id' => $id],'*');
        $articleModel = new ArticleModel();
        // 将分类修改为单页
        if($data['type'] == 3){
            // 判断当前分类是否存在文章
            $articleDetail = $articleModel->getDataDetail("FIND_IN_SET($id,category_ids)");
            if($articleDetail){
                return [
                    'status' => 0,
                    'msg' => '当前分类下存在文章无法修改为详情',
                ];
            }else{
                // 添加文章
                $articleData = [
                    'title' => $data['name'],
                    'category_ids' => $id,
                    'miniapp_id' => $cateogryDetail['miniapp_id'],
                ];
                $articleModel->save($articleData);
            }
        }
        // 判断分类类型
        if($data['type'] != 3 && $cateogryDetail['type'] == 3){
            $articleModel->deleteData(['category_ids' => $data['id']]);
        }

        // 判断是否存在父id
        if(empty($data['pid'])){
            $newPath = '0,'.$id;
        }else{
            $parentPath = $categoryModel->where('id',$pid)->value('path');
            $newPath = $parentPath.','.$id;
        }
        $data['path'] = $newPath;
        $data['level'] = count(explode(',',$newPath)) -2;
        $condition[] = ['path','like',$cateogryDetail['path'].",%"];
        $childrenCategory = $categoryModel->getDataList($condition,'id desc','id,path');
        if ($childrenCategory) {
            foreach($childrenCategory as $category){
                $childPath = str_replace($cateogryDetail['path'] . ',', $newPath . ',', $category['path']);
                $level = count(explode(',',$category['path'])) -2;
                $categoryModel->update(['path' => $childPath,'level' => $level], ['id' => $category['id']]);
            }
        }

        $categoryModel->allowField(true)->update($data,['id' => $data['id']]);
        return [
            'status' => 1,
        ];
    }


    // 分类树
    public static function getCate($pid=0, &$result=[], $blank=0)
    {
        $categoryModel = new CategoryModel();
        $res = $categoryModel->where(['pid'=>$pid])->select();
        $blank += 2;
        foreach ($res as $key => $value) {
            $cate_name = '|--'.$value->name;
            $value->cate_name = str_repeat('&nbsp;',$blank).$cate_name;
            $result[] = $value;
            self::getCate($value->id, $result, $blank);
        }
        return $result;
    }

}